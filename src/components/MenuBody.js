import React, {Component} from 'react';
import {View, Text, StyleSheet, TouchableOpacity} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome5';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';

class MenuBody extends Component {
  render() {
    return (
      <View style={styles.networkContainer} {...this.props}>
        <TouchableOpacity onPress={this.props.onPress}>
          <View style={{flexDirection: 'row',alignItems: "center",marginBottom: hp("2%"), height: 50}}>
            <View style={{flex:1,alignItems: "center"}}>
            <Icon
              name={this.props.firstIconName}
              size={hp("3.5%")}
            />
            </View>
            <View style={{flex:4}}>
            <Text style={styles.textStyle}>
              {' '}
              {this.props.textName}
            </Text>
            </View>
      
            <View style={{flex:1,alignItems: "center"}}>
            <Icon 
            name={this.props.rightIconDown}
            size={hp("3%")}
            />
            </View>
          </View>
        </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  networkContainer: {
    flex: 1,
  },
  textStyle: {
    fontSize: 20
  }
});

export default MenuBody;
