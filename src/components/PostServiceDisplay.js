import React, { Component } from 'react';
import { View, Text,StyleSheet,TouchableOpacity } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome5';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import PostServiceSubDisplay from './PostServiceSubDisplay';



class PostServiceDisplay extends Component {
  constructor(props) {
    super(props);
    this.state = {

        show: false,
        icon_down: true,
        icon_up: false,
    };
  }


  showData = () => {
    this.setState({
      show: true,
      icon_down: false,
      icon_up: true,
    });
  };

  hideData = () => {
    this.setState({
      show: false,
      icon_down: true,
      icon_up: false,
    });
  };

  render() {
  
    return (
      <View>
         <View style={styles.bodyStyle}>
            <View style={{flex: 1, alignItems: 'center'}}>
              <Icon
                name={this.props.iconName}
                size={22}
                style={{
                  backgroundColor: '#F5F5F5',
                  borderRadius: 300,
                  padding: 10,
                }}
              />
            </View>
            <View style={{flex: 3}}>
              <Text style={{fontSize: 22, fontWeight: '800'}}>{this.props.serviceName}</Text>
            </View>

            <View style={{alignItems: 'flex-end', flex: 1.5}}>
              {this.state.icon_up === true ? (
                <Icon
                  name="angle-up"
                  size={20}
                  onPress={this.hideData}
                  style={{marginRight: wp('5%'), padding: 20}}
                />
              ) : null}
              {this.state.icon_down === true ? (
                <Icon
                  name="angle-down"
                  size={20}
                  onPress={this.showData}
                  style={{marginRight: wp('5%'), padding: 20}}
                />
              ) : null}
            </View>
          </View>
          {this.state.show === true ? (
           <View>
           <PostServiceSubDisplay  {...this.props}  subIconName={this.props.subIconNameone} />
           <PostServiceSubDisplay  {...this.props}  subIconName={this.props.subIconNametwo}/>
           <PostServiceSubDisplay  {...this.props}  subIconName={this.props.subIconNamethree}/>
           </View>    
            
          ) : 
           
               null
        }
      </View>
    );
  }
}

const styles = StyleSheet.create({
    bodyStyle: {
        flexDirection: 'row',
        // backgroundColor: "orange",
        marginTop: hp('2%'),
        alignItems: 'center',
      },
})

export default PostServiceDisplay;
