import React, {Component} from 'react';
import {
  Text,
  View,
  TouchableOpacity,
  StyleSheet,
  TextInput,
  ScrollView,
  Image
} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome5';
import Header from './Header';
import ImagePicker from 'react-native-image-picker';
import {connect} from 'react-redux';
import { SignUpAction } from "../action";



import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';

export class BasicInfo extends Component {
  constructor(props) {
    super(props);
    this.state = {
      phoneNumber: '',
      userName: '',
      userLocation: {
        latitude: '',
        longitude: '',
        locationName: '',
      },
      ImageSource: null,
      phoneNumberform: '',
      userName: ''
    };
    this.countryCode = this.props.navigation.getParam('countryCode')
    this.phoneNumber = this.props.navigation.getParam('phoneNumber')
    
  }

   SignUpUser = ()=>{
    const newstate = {
      countryCode : this.countryCode,
      phoneNumber: this.phoneNumber,
      profileImageURL: this.state.ImageSource.uri,
      userName: this.state.userName,
      "languageId": '1',
      "verifiedFacebook": true,
      "verifiedGoogle": true,
      "uploadIdCard": "string",
      "userLocation": {
        "latitude": 0,
        "longitude": 0,
        "locationName": "string"
      },
      "userDevice": {
        "deviceType": "string",
        "deviceId": "string"
      },
      "applozicId": "string",
      "applozicStatus": "string",
      "applozicToken": "string"
    }
      this.props.SignUpAction(newstate)
      this.props.navigation.navigate("BuildTrust")
    
  }


  selectPhotoTapped = () => {
    const options = {
      quality: 1.0,
      maxWidth: 500,
      maxHeight: 500,
      storageOptions: {
        skipBackup: true,
      },
    };

    ImagePicker.showImagePicker(options, response => {
      console.log('Response = ', response);

      if (response.didCancel) {
        console.log('User cancelled photo picker');
      } else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      } else {
        let source = {uri: response.uri};

        // You can also display the image using data:
        // let source = { uri: 'data:image/jpeg;base64,' + response.data };

        this.setState({
          ImageSource: source,
        });
      }
    });
  }

  render() {
    // console.warn(this.state.phoneNumberform)
    const {navigation} = this.props;
    const phoneNumber = navigation.getParam('phoneNumber', '');
    return (
      <View style={styles.container}>
        <Header
          text="Sign Up 2/3- Basic Info"
          name="times"
          leftIconStyle={{marginLeft: 18}}
        />
        <View style={styles.bodyContainer}>
          <ScrollView style={{flex: 1}}>
            <View
              style={{
                flex: 1,
                justifyContent: 'center',
                alignItems: 'center',
                height: hp('10%'),
              }}>
              <Text style={{fontSize: hp('3%')}}>Required</Text>
            </View>
            <View style={styles.imageContainer}>
              <View>
                <TouchableOpacity onPress={this.selectPhotoTapped}>
                  {this.state.ImageSource === null ? (
                    <Icon
                      name="user-circle"
                      size={hp('7%')}
                      style={{
                        backgroundColor: '#e9e9e2',
                        borderRadius: 300,
                        padding: 18,
                      }}
                    />
                  ) : (
                    <Image source={this.state.ImageSource} style={{width: 90,height: 90,borderRadius: 200}}/>
                  )}
                </TouchableOpacity>
              </View>
              <View style={{position: 'absolute', left: wp('55%')}}>
                <Icon
                  name="camera-retro"
                  size={15}
                  style={{
                    color: 'white',
                    backgroundColor: '#258489',
                    borderRadius: 300,
                    padding: 7,
                  }}
                />
              </View>
            </View>
            <View style={styles.inputContainer}>
              <TextInput
                placeholder="Phone Number"
                onChangeText={phoneNumberform => this.setState({phoneNumberform})}
                style={{
                  backgroundColor: '#E0E0E0',
                  borderRadius: 5,
                  padding: 15,
                  borderBottomWidth: 1.5,
                  borderBottomColor: '#258489',
                }}
              />
            {this.state.phoneNumberform === phoneNumber ? <Text>Verfied</Text> : null}
              <TextInput
                placeholder="Username"
                onChangeText={userName => this.setState({userName})}
                style={{
                  backgroundColor: '#E0E0E0',
                  borderRadius: 5,
                  marginTop: hp('2%'),
                  padding: 15,
                  borderBottomWidth: 1.5,
                  borderBottomColor: '#258489',
                }}
              />
              <Text
                style={{
                  marginLeft: wp('2%'),
                  marginTop: hp('0.5%'),
                  color: '#258489',
                }}>
                Enter your username
              </Text>
            </View>
            <View style={styles.locationContainer}>
              <View style={{alignItems: 'center'}}>
                <Text style={{fontSize: 20}}> Optional</Text>
              </View>

              <View style={{margin: 20}}>
                <TextInput
                  placeholder="Your Location"
                  style={{
                    backgroundColor: '#E0E0E0',
                    borderRadius: 5,
                    padding: 15,
                    borderBottomWidth: 1.5,
                    borderBottomColor: '#258489',
                  }}
                />
                <Text
                  style={{
                    marginLeft: wp('2%'),
                    marginTop: hp('0.5%'),
                    color: 'grey',
                  }}>
                  System can then show you posts nearby
                </Text>
              </View>
            </View>
            <View style={styles.endContainer}>
              <TouchableOpacity
                onPress={this.SignUpUser}
                style={{
                  backgroundColor: '#ff4d88',
                  borderRadius: 200,
                  padding: 15,
                  alignItems: 'center',
                  flexDirection: 'row',
                }}>
                <Icon name="check" color="white" size={hp('2.5%')} />
                <Text
                  style={{
                    fontSize: hp('2.5%'),
                    color: 'white',
                    marginLeft: wp('2.5%'),
                  }}>
                  SIGN UP
                </Text>
              </TouchableOpacity>
            </View>
          </ScrollView>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  bodyContainer: {
    flex: 7,
  },
  imageContainer: {
    alignItems: 'center',
    flex: 2,
    height: hp('15%'),
  },
  inputContainer: {
    flex: 4,
    height: hp('25%'),
    justifyContent: 'center',
    marginLeft: 20,
    marginRight: 20,
    marginBottom: 20,
  },
  locationContainer: {
    flex: 3,
    height: hp('18%'),
  },
  endContainer: {
    flex: 2,
    alignItems: 'flex-end',
    margin: 20,
    height: hp('10%'),
    justifyContent: 'center',
  },
});

export default connect(null,{SignUpAction})(BasicInfo);
