import React, {Component} from 'react';
import {
  View,
  Text,
  StyleSheet,
  ScrollView,

} from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import Header from './Header';
import PostServiceDisplay from './PostServiceDisplay';

class PostService extends Component {
  render() {
    console.log('Post Service -', this.props);
    return (
      <View style={styles.container}>
        <Header
          text="Post Service"
          name="times"
          onPress={() => this.props.navigation.navigate('Menu')}
          leftIconStyle={{marginLeft: 18}}
        />
      
        <View style={styles.textContainer}>
          <Text style={styles.textContainerText}>
            What services can you provide ?
          </Text>
        </View>
        <View style={styles.bodyContainer}>
        <ScrollView style={{flex:1}}>
          <PostServiceDisplay
            {...this.props}
            serviceName="Home Services"
            iconName="building"
            subIconNameone="Childcare"
            subIconNametwo="Home assistance"
            subIconNamethree="Cooking Service"
          />
          <PostServiceDisplay
            {...this.props}
            serviceName="Education"
            iconName="graduation-cap"
            subIconNameone="Academic Tutor"
            subIconNametwo="Foreign Language tutor"
            subIconNamethree="Music Tutor"
          />
             </ScrollView>
        </View>
     
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  bodyContainer: {
    flex: 7,
  },
  textContainer: {
    flex: 1,
    // backgroundColor: "orange",
    justifyContent: 'center',
    alignItems: 'center',
  },
  textContainerText: {
    fontSize: 23,
    fontWeight: '900',
  },
  bodyStyle: {
    flexDirection: 'row',
    // backgroundColor: "orange",
    marginTop: hp('2%'),
    alignItems: 'center',
  },
});

export default PostService;
