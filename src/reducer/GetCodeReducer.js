export default(state=[], action) => {
    switch (action.type) {
        case "GET_CODE":
            return action.payload
        case "GET_CODE_SIGNUP":
            return action.payload    
         default: return state;       
    }
}