import React, {Component} from 'react';
import {
  View,
  Text,
  StyleSheet,
  Dimensions,
  ScrollView,
  TouchableOpacity,
  Alert,
} from 'react-native';
import Header from './Header';
import NetworkView from './MenuBody/NetworkView';
import {TabView, SceneMap, TabBar} from 'react-native-tab-view';
import PostServiceOverview from '../components/PostServicebody/PostServiceOverview';
import JobsTab from './ViewProfile/JobsTab';
import ReviewTab from './ViewProfile/ReviewTab';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import PostServicePricing from '../components/PostServicebody/PostServicePricing';
import PostServiceHeading from '../components/PostServicebody/PostServiceHeading';
import PostServiceDetails from '../components/PostServicebody/PostServiceDetails';
import PostServiceReviewTab from './PostServicebody/PostServiceReviewTab';

const FirstRoute = () => (
  <View style={{flex: 1}}>
    <PostServiceOverview />
  </View>
);

const SecondRoute = () => (
  <View style={{flex: 1}}>
    <PostServiceHeading headingName="Pricing" />
    <PostServicePricing />
  </View>
);

const ThirdRoute = () => (
  <View style={{flex: 1}}>
    <PostServiceHeading headingName="Details" />
    <PostServiceDetails />
  </View>
);

const FourthRoute = () => (
  <View style={{flex: 1}}>
    <PostServiceHeading headingName="Reviews" />
    <PostServiceReviewTab />
  </View>
);

const FifthRoute = () => (
  <View style={{flex: 1}}>
    <Text>fifth</Text>
  </View>
);

class ServicePostDescription extends Component {
  constructor(props) {
    super(props);
    this.state = {
      index: 0,
      routes: [
        {key: 'first', title: 'OVERVIEW'},
        {key: 'second', title: 'PRICING'},
        {key: 'third', title: 'DETAILS'},
        {key: 'fourth', title: 'REVIEWS'},
        {key: 'fifth', title: 'LOCATION'},
      ],
    };
  }

  componentDidMount = () => {
    // Alert.alert(
    //     'Service submitted',
    //     'Your service is now displayed on the map. Whoever interested will send you',
    //     [
    //       {text: 'GOT IT', onPress: () => console.log('OK Pressed') },
    //     ],
    //     {cancelable: false},
    //   );
  };

  render() {
    
    return (
      <View style={styles.container}>
        <Header
          name="angle-left"
          onPress={() => this.props.navigation.navigate('Menu')}
          middleFirstIcon="share-alt"
          middleSecondIcon="bookmark"
          nameRight="ellipsis-v"
        />
        {this.state.index === 0 ? (
          <View style={styles.cardContainer}></View>
        ) : null}
        <View style={styles.bodyContainer}>
          <TabView
            navigationState={this.state}
            renderScene={SceneMap({
              first: FirstRoute,
              second: SecondRoute,
              third: ThirdRoute,
              fourth: FourthRoute,
              fifth: FifthRoute,
            })}
            onIndexChange={index => this.setState({index})}
            initialLayout={{width: Dimensions.get('window').width}}
            renderTabBar={props => (
              <TabBar
                {...props}
                indicatorStyle={{backgroundColor: '#00b38f'}}
                style={{backgroundColor: 'white'}}
                renderLabel={({route}) => (
                  <Text style={{fontWeight: '500', fontSize: hp('1.7%')}}>
                    {route.title}
                  </Text>
                )}
              />
            )}
            // swipeEnabled={false}
          />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  cardContainer: {
    flex: 3,
    marginTop: 20,
    // backgroundColor: 'orange',
  },
  bodyContainer: {
    flex: 7,
    // backgroundColor: 'red',
  },
  scene: {
    flex: 1,
  },
});

export default ServicePostDescription;
