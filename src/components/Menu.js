import React, {Component} from 'react';
import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  ScrollView,
} from 'react-native';
import Header from '../components/Header';
import Icon from 'react-native-vector-icons/FontAwesome5';
import MenuBody from './MenuBody';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';

class Menu extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showSettings: false,
      showHelp: false,
    };
  }

  showDataSettings = () => {
    this.setState({
      showSettings: true,
    });
  };

  hideDataSettings = () => {
    this.setState({
      showSettings: false,
    });
  };

  showDataHelp = () => {
    this.setState({
      showHelp: true,
    });
  };

  hideDataHelp = () => {
    this.setState({
      showHelp: false,
    });
  };

  render() {
    console.log(this.state.show);
    return (
      <View style={styles.container}>
        <Header text="Menu" />
        <View style={styles.profileContainer}>
          <View style={styles.profileBodyContianer}>
            <Icon name="user-circle" size={30} style={styles.profileIcon} />
            <View style={styles.profileTextContainer}>
              <Text style={{fontSize: 20}}>Chaitanya</Text>
              <TouchableOpacity
                onPress={() => this.props.navigation.navigate('ViewProfile')}>
                <Text
                  style={{fontSize: 16, color: '#009973', fontWeight: '500'}}>
                  VIEW PROFILE
                </Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
        <View style={styles.bodyContainer}>
          <ScrollView style={{flex: 1}}>
            <MenuBody
              firstIconName="users"
              textName="My Network"
              onPress={() => this.props.navigation.navigate('Network')}
            />
            <MenuBody firstIconName="envelope-open" textName="My Reviews" />
            <MenuBody
              firstIconName="clipboard-list"
              textName="My Services"
              onPress={() => this.props.navigation.navigate('Services')}
            />
            <MenuBody firstIconName="tag" textName="My Job Posts" />
            <MenuBody firstIconName="heart" textName="My Favourites" />
            {this.state.showSettings === false ? (
              <MenuBody
                firstIconName="cog"
                textName="Settings"
                rightIconDown="angle-down"
                onPress={this.showDataSettings}
              />
            ) : (
              <MenuBody
                firstIconName="cog"
                textName="Settings"
                rightIconDown="angle-up"
                onPress={this.hideDataSettings}
              />
            )}
            {this.state.showSettings === true ? (
              <View>
                <MenuBody
                  firstIconName="user-circle"
                  textName="Account Settings"
                />
                <MenuBody
                  firstIconName="bell"
                  textName="Notificaitons Settings"
                />
                <MenuBody firstIconName="sms" textName="Message Settings" />
                <MenuBody firstIconName="eye" textName="Privacy Settings" />
              </View>
            ) : null}
            <MenuBody firstIconName="globe" textName="Language" />
            {this.state.showHelp === false ? (
              <MenuBody
                firstIconName="question-circle"
                textName="Help"
                rightIconDown="angle-down"
                onPress={this.showDataHelp}
              />
            ) : (
              <MenuBody
                firstIconName="question-circle"
                textName="Help"
                rightIconDown="angle-up"
                onPress={this.hideDataHelp}
              />
            )}

            {this.state.showHelp === true ? (
              <View>
                <MenuBody firstIconName="home" textName="About Helpa" />
                <MenuBody firstIconName="phone" textName="Contact Helpa" />
              </View>
            ) : null}

            <MenuBody firstIconName="file-alt" textName="Terms & Privacy" />
            <MenuBody firstIconName="flag" textName="Report" />
            <MenuBody
              firstIconName="sign-out-alt"
              textName="Log Out"
              onPress={() => this.props.navigation.navigate('SignUpOne')}
            />
          </ScrollView>
        </View>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  profileContainer: {
    // flex: 1.3,
    height: 85,
    // backgroundColor: "yellow"
  },
  profileBodyContianer: {
    flexDirection: 'row',
    flex: 1,
    borderBottomColor: 'grey',
    borderBottomWidth: 0.5,
    marginLeft: wp('2%'),
    marginBottom: hp('2%'),
  },
  profileIcon: {
    marginLeft: wp('5%'),
    marginTop: wp('5%'),
  },
  profileTextContainer: {
    marginTop: 7,
    marginLeft: 20,
  },
  bodyContainer: {
    flex: 9,
  },
});

export default Menu;
