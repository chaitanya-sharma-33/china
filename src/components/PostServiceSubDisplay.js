import React, { Component } from 'react';
import { View, Text,TouchableOpacity,StyleSheet } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome5';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';

class PostServiceSubDisplay extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  render() {
    
    

    return (
     <View style={[styles.bodyStyle, {display: 'flex'}]}  {...this.props}>
       <View style={{flex: 1, alignItems: 'center'}}></View>
              <View style={{flex: 3}}>
                <Text style={{fontSize: 20, fontWeight: '800'}}>{this.props.subIconName}</Text>
              </View>
              <TouchableOpacity style={{flex: 1}} onPress={() => this.props.navigation.navigate("PostServiceCategory", {
                titleName: this.props.subIconName
              })}>
                <View
                  style={{
                    alignItems: 'flex-end',
                    marginRight: wp('1.4%'),
                  }}>
                       <Icon
                  name="angle-right"
                  size={20}
                  style={{marginRight: wp('5%'), padding: 20}}
                />
                  </View>
              </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
    bodyStyle: {
        flexDirection: 'row',
        // backgroundColor: "orange",
        marginTop: hp('2%'),
        alignItems: 'center',
      },
})

export default PostServiceSubDisplay;
