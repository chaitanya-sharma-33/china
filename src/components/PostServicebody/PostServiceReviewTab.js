import React, {Component} from 'react';
import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  ActivityIndicator,
  Image,
  ScrollView
} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome5';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import StarRating from 'react-native-star-rating';

class PostServiceReview extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <ScrollView>
      <View style={styles.container}>
        <View style={styles.reviewDataContainer}>
          <TouchableOpacity style={{flexDirection: 'row'}}>
            <View style={styles.IconViewContainer}>
              <Image
                source={{
                  uri:
                    'http://raisegrate.com/wp-content/uploads/2018/03/mp4.jpg',
                }}
                style={{width: 38, height: 38, borderRadius: 200}}
              />
            </View>
            <View style={styles.bodyContainer}>
              <View style={styles.bodyInnerContainer}>
                <Text style={styles.personNameStyle}>Peter Pan</Text>
                <StarRating
                  disabled={false}
                  maxStars={5}
                  starSize={10}
                  rating={5}
                  starStyle={{marginLeft: wp('1%')}}
                  fullStarColor="#18c1ba"
                />
                <View>
                  <TouchableOpacity
                    style={{marginRight: wp('2%'), marginBottom: hp('2%')}}>
                    <Text style={{marginTop: 10}}>
                      The place is exactly like the pictures. The location is
                      absolutely fabulous and I advise you to try restaurants on
                      their recommended list.
                      <Text
                        style={{
                          fontSize: 15,
                          color: '#18c1ba',
                          marginLeft: 20,
                        }}>
                        (...MORE){' '}
                      </Text>{' '}
                    </Text>
                  </TouchableOpacity>
                </View>
              </View>
              <TouchableOpacity style={{flexDirection: 'row'}}>
                <View style={styles.IconViewContainer2}>
                  <Image
                    source={{
                      uri:
                        'http://raisegrate.com/wp-content/uploads/2018/03/mp4.jpg',
                    }}
                    style={{width: 38, height: 38, borderRadius: 200}}
                  />
                </View>
                <View style={styles.bodyInnerContainer2}>
                  <View style={{flexDirection: 'row', alignItems: 'center'}}>
                    <Text style={styles.personNameStyle}>
                     Peter Pan
                    </Text>
                    <StarRating
                      disabled={false}
                      maxStars={5}
                      starSize={10}
                      rating={5}
                      starStyle={{marginLeft: wp('1%')}}
                      fullStarColor="#18c1ba"
                    />
                  </View>
                  <View>
                    <TouchableOpacity
                      style={{marginRight: wp('2%'), marginBottom: hp('2%')}}>
                      <Text style={{marginTop: 10}}>
                      The place is exactly like the pictures. The location is
                      absolutely fabulous and I advise you to try restaurants on
                      their recommended list.
                        <Text
                          style={{
                            fontSize: 15,
                            color: '#18c1ba',
                            marginLeft: 20,
                          }}>
                          (...MORE){' '}
                        </Text>{' '}
                      </Text>
                    </TouchableOpacity>
                  </View>
                </View>
              </TouchableOpacity>
            </View>
            <View style={styles.lastIconContainer}>
              <Icon name="ellipsis-v" size={18} />
            </View>
          </TouchableOpacity>
        </View>
      </View>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
  },
  reviewDataContainer: {
    flex: 1,
    marginLeft: wp('8%'),
    borderWidth: 1,
    borderColor: 'grey',
    marginRight: wp('8%'),
    borderRadius: 7,
    marginBottom: hp('2%'),
    justifyContent: 'center',
  },
  IconViewContainer: {
    flex: 1,
    alignItems: 'center',
    marginTop: hp('1.5%'),
    marginLeft: wp('1.8%'),
  },
  IconViewContainer2: {
    flex: 1,
    flexWrap: 'wrap',
    backgroundColor: 'green',
    marginLeft: wp('1%'),
    marginRight: wp('1.5%'),
  },
  bodyContainer: {
    flex: 7,
    marginBottom: hp('1%'),
    alignItems: 'center',
  },
  personNameStyle: {
    fontSize: hp('2.5%'),
    fontWeight: '500',
  },
  personNameIcon: {
    color: 'red',
    marginLeft: wp('2%'),
  },
  innerTextStyle: {
    marginLeft: wp('2%'),
  },
  circleStyle: {
    backgroundColor: 'black',
    borderRadius: 300,
    height: '18%',
    marginLeft: wp('2%'),
  },
  innerIconStyleStar: {
    marginLeft: -2,
    marginBottom: hp('4%'),
  },
  lastIconContainer: {
    marginTop: hp('2.5%'),
    marginRight: wp('2%'),
  },
  bodyInnerContainer: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    alignItems: 'center',
    marginLeft: wp('4%'),
    marginTop: hp('1.5%'),
  },
  bodyInnerContainer2: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    alignItems: 'center',
    marginLeft: wp('10%'),
  },
});

export default PostServiceReview;
