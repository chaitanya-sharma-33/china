import React, {Component} from 'react';
import {View, Text, TouchableOpacity, StyleSheet} from 'react-native';
import Header from './Header';
import Icon from 'react-native-vector-icons/FontAwesome5';

import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';

class BuildTrust extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <View style={styles.container}>
        <Header
          text="Sign Up 3/3- Build Trust"
          name="times"
          leftIconStyle={{marginLeft: 18}}
        />
        <View style={{flex: 7}}>
          <View
            style={{
              height: hp('10%'),
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <Text style={{fontSize: 20, fontWeight: '900'}}>
              Hey, you can do it later!
            </Text>
          </View>
          <View
            style={{
              flex:1,
              flexDirection: 'row',
            }}>
            <View
              style={{
                flexDirection: 'row',
                flex: 4,
                alignItems: 'center',
                justifyContent: 'space-around',
              }}>
              <Icon
                name="globe"
                size={20}
                style={{
                  backgroundColor: '#e9e9e2',
                  borderRadius: 200,
                  padding: 10,
                  marginLeft: wp("1.5%")

                }}
              />
              <Text style={{fontSize: 20,marginLeft: wp("2%")}}>Connect with facebook</Text>
            </View>
            <TouchableOpacity
              style={{
                flex: 2,
                alignItems: 'center',
                flexDirection: 'row',
                justifyContent: 'flex-end',
              }}>
              <View style={{marginRight: wp('3%')}}>
                <Icon name="angle-right" size={25} />
              </View>
            </TouchableOpacity>
          </View>
          <View
            style={{
              flex:1,
              flexDirection: 'row',
            }}>
            <View
              style={{
                flexDirection: 'row',
                flex: 4,
                alignItems: 'center',
                justifyContent: 'space-around',
              }}>
              <Icon
                name="globe"
                size={20}
                style={{
                  backgroundColor: '#e9e9e2',
                  borderRadius: 200,
                  padding: 10,
                  marginLeft: wp("1.5%")
                }}
              />
              <Text style={{fontSize: 20,marginLeft: wp("2%")}}>Connect with google</Text>
            </View>
            <TouchableOpacity
              style={{
                flex: 2.5,
                alignItems: 'center',
                flexDirection: 'row',
                justifyContent: 'flex-end',
              }}>
              <View style={{marginRight: wp('3%')}}>
                <Icon name="angle-right" size={25} />
              </View>
            </TouchableOpacity>
          </View>
          <View
            style={{
              flex:1,
              flexDirection: 'row',
            }}>
            <View
              style={{
                flexDirection: 'row',
                flex: 4,
                alignItems: 'center',
                justifyContent: 'space-around',
              }}>
              <Icon
                name="id-card-alt"
                size={20}
                style={{
                  backgroundColor: '#e9e9e2',
                  borderRadius: 200,
                  padding: 10,
                  marginLeft: wp("1.5%")

                }}
              />
              <Text style={{fontSize: 20,marginLeft: wp("2%")}}>Upload ID Card</Text>
            </View>
            <TouchableOpacity
              style={{
                flex: 4,
                alignItems: 'center',
                flexDirection: 'row',
                justifyContent: 'flex-end',
              }}>
              <View style={{marginRight: wp('2%')}}>
                <Icon name="cloud-upload-alt" size={25} />
              </View>
            </TouchableOpacity>
          </View>
          
          
          
          
          <View
            style={{
              flex: 6,
              justifyContent: "flex-end",
              alignItems: "flex-end",
             
            }}>
                 <TouchableOpacity
              onPress={() => this.props.navigation.navigate('Home')}
              style={{
                backgroundColor: '#ff4d88',
                borderRadius: 200,
                padding: 15,
                alignItems: 'center',
                flexDirection: 'row',
                margin: 20,
                elevation: 15

              }}>
              <Icon name="check" color="white" size={hp('2.5%')} />
              <Text
                style={{
                  fontSize: hp('2.5%'),
                  color: 'white',
                  marginLeft: wp('2.5%'),
                }}>
                SIGN UP
              </Text>
            </TouchableOpacity>
            </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});

export default BuildTrust;
