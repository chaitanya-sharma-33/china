import React, {Component} from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  StyleSheet,
  Image,
  ActivityIndicator,
} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome5';
import RadioForm, {
  RadioButton,
  RadioButtonInput,
  RadioButtonLabel,
} from 'react-native-simple-radio-button';
import Header from './Header';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {connect} from 'react-redux';
import {getCountryCodeAction} from '../action';

var radio_props = [{label: 'param1', value: 0}, {label: 'param2', value: 1}];

class ZipCodeSignUp extends Component {

  

  componentDidMount = () => {
    this.props.getCountryCodeAction();
  };


  renderList = () => {
    if (this.props.display.length === 0) {
      return <ActivityIndicator size="large" color="blue" />;
    } else {
      return this.props.display.map((item, i) => {
        return (
          <View key={i} style={styles.displayDataContainer}>
            <View
              style={{
                flex: 6,
                flexDirection: 'row',
                alignItems: 'center',
                marginTop: hp('2%'),
              }}>
              <View
                style={{
                  flex: 2,
                  alignItems: 'center',
                }}>
                <Icon name="globe" size={25} />
              </View>
              <View
                style={{
                  flex: 6,

                  flexDirection: 'row',
                }}>
                <Text style={{fontSize: hp('2%')}}>{item.name}</Text>
                <Text style={{fontSize: hp('2%'), marginLeft: wp('2%')}}>
                  ({item.code})
                </Text>
              </View>
            </View>
            <View style={{flex: 2, marginTop: hp('2%')}}>
              <TouchableOpacity onPress={() => this.props.navigation.navigate("SignUpOne", {
                countryName: item.name,
                code: item.code
              })}>
                <Text> button</Text>
              </TouchableOpacity>
            </View>
          </View>
        );
      });
    }
  };

  render() {

    return (
      <View style={styles.container}>
        <Header
          name="angle-left"
          text="Sign Up 1/3- Verify Phone"
          onPress={() => this.props.navigation.navigate('SignUpOne')}
        />
        <View style={styles.bodyTextContainer}>
          <Text style={styles.bodyText}>Choose your zip code</Text>
        </View>
        <View style={styles.radioButton}>{this.renderList()}</View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  headerContainer: {
    flexDirection: 'row',
    // backgroundColor: "red",
    flex: 0.8,
    alignItems: 'center',
    borderBottomWidth: 0.5,
    borderBottomColor: 'grey',
  },
  headerText: {
    fontSize: 21,
    fontWeight: '400',
    marginLeft: 40,
  },
  bodyTextContainer: {
    flex: 1,
    //   backgroundColor: "orange",
    justifyContent: 'center',
    alignItems: 'center',
  },
  bodyText: {
    fontSize: 20,
    fontWeight: '400',
  },

  radioButton: {
    flex: 6,
  },
  displayDataContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    // backgroundColor: "orange",
  },
});

const mapStateToProps = state => {
  return {
    display: state.display,
    getSignUpCode: state.getSignUpCode
  };
};

export default connect(
  mapStateToProps,
  {getCountryCodeAction},
)(ZipCodeSignUp);
