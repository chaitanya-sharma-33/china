import React, {Component} from 'react';
import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  TextInput,
  Image,
} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome5';
import Header from './Header';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';

import {verifyOTPAction} from '../action';
import {connect} from 'react-redux';

class VerificationCodeSignUp extends Component {
  constructor(props) {
    super(props);
    this.state = {
        otp:''
    };

    this.countryCode = this.props.navigation.getParam('countryCode')
    this.phoneNumber = this.props.navigation.getParam('phoneNumber')
  }

  verifyOTP = ()=>{
    const newstate = {
      countryCode : this.countryCode,
      phoneNumber: this.phoneNumber,
      otp: this.state.otp
    }
      this.props.verifyOTPAction(newstate)
      this.props.navigation.navigate("BasicInfo", {
        countryCode: this.countryCode,
        phoneNumber: this.phoneNumber,
      
      })
    
  }
  

  render() {
    console.log(this.state.otp)

    return (
      <View style={styles.container}>
        <Header
          text="Sign Up 1/3- Verify Phone"
          name="times"
          leftIconStyle={{marginLeft: 18}}
        />
        <View style={styles.bodyContainer}>
          <View style={styles.bodyTextContainer}>
            <Text style={styles.bodyFirstText}>Enter Verification code</Text>
          </View>

          <View style={styles.bodyInputContainer}>
            {/* <Text style={{marginLeft:5}}>Phone number</Text> */}
            <TextInput
              style={styles.bodyTextInput}
              placeholder="Verification Code"
              placeholderTextColor="#188a6e"
              onChangeText={otp => this.setState({otp})}
            />
          </View>
          <View
            style={{
              margin: 25,
              flexDirection: 'row',
              justifyContent: 'space-between',
              alignItems: 'center',
            }}>
            <TouchableOpacity>
              <Text
                style={{
                  fontSize: hp('2%'),
                  color: '#188a6e',
                  fontWeight: '800',
                }}>
                RESEND CODE
              </Text>
            </TouchableOpacity>
            <TouchableOpacity
              onPress={this.verifyOTP}
              style={{
                backgroundColor: '#ff4d88',
                borderRadius: 200,
                padding: 15,
                alignItems: 'center',
                flexDirection: 'row',
              }}>
              <Icon name="check" color="white" size={hp('2.5%')} />
              <Text
                style={{
                  fontSize: hp('2.5%'),
                  color: 'white',
                  marginLeft: wp('2.5%'),
                }}>
                VERIFY
              </Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  bodyContainer: {
    flex: 8,
  },
  bodyTextContainer: {
    alignItems: 'center',
    flex: 2,
    justifyContent: 'center',
    alignItems: 'center',
  },
  bodyFirstText: {
    fontSize: 23,
    fontWeight: '400',
  },
  bodyZipContainer: {
    flex: 2,
    flexDirection: 'row',
    alignItems: 'center',
  },

  bodyZipText: {
    fontSize: hp('3%'),
  },
  bodyZipTextSecond: {
    marginLeft: wp('1.3%'),
  },

  bodyZipIcon: {
    marginLeft: wp('14%'),
  },
  bodyInputContainer: {
    flex: 6,
    marginTop: hp('2%'),
    marginRight: wp('3%'),
    marginLeft: wp('3%'),
  },
  bodyTextInput: {
    borderBottomColor: 'black',
    borderBottomWidth: 2,
    backgroundColor: '#e4e4e4',
    borderTopLeftRadius: 4,
    borderTopRightRadius: 4,
    borderBottomColor: '#188a6e',
  },
});

export default connect(null,{verifyOTPAction})(VerificationCodeSignUp);
