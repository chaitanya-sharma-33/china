import {createAppContainer} from 'react-navigation';
import {createStackNavigator} from 'react-navigation-stack';
import {createBottomTabNavigator} from 'react-navigation-tabs';
import Login from '../components/Login';
import SignUpOne from '../components/SignUpOne';
import ZipCodeLogin from '../components/ZipCodeLogin';
import Home from '../components/Home';
import Message from '../components/Message';
import Notification from '../components/Notificaiton';
import Menu from '../components/Menu';
import Search from '../components/Search';
import Icon from 'react-native-vector-icons/FontAwesome5';
import React from 'react';
import ViewProfile from '../components/ViewProfile';
import Network from '../components/MenuBody/Network';
import Favourites from '../components/MenuBody/Favourites';
import JobPost from '../components/MenuBody/JobPost';
import Reviews from '../components/MenuBody/Reviews';
import Language from '../components/MenuBody/Language';
import Services from '../components/MenuBody/Services';
import AddPost from '../components/AddPost';
import CardProfile from '../components/MenuBody/CardProfile';
import VerificationCodeLogin from '../components/VerificationCodeLogin';
import ZipCodeSignUp from '../components/ZipCodeSignUp';
import VerificationCodeSignUp from '../components/VerificationCodeSignUp';
import BasicInfo from '../components/BasicInfo';
import BuildTrust from '../components/BuildTrust';
import PostService from '../components/PostService';
import PostServiceCategory from '../components/PostServiceCategory';
import PostServiceDisplay from '../components/PostServiceDisplay';
import PostServiceSubDisplay from '../components/PostServiceSubDisplay';
import PostServiceLocation from '../components/PostServiceLocation';
import PostServicePricing from '../components/PostServicePricing';
import PostServiceMinMax from '../components/PostServiceMinMax';
import PostServiceReview from '../components/PostServiceReview';
import ServicePostDescription from '../components/ServicePostDescription';


const homeBottomTab = createBottomTabNavigator(
  {
    Search: {screen: Search},
    Message: {screen: Message},
    Notification: {screen: Notification},
    Menu: {screen: Menu},
  },

  {
    defaultNavigationOptions: ({navigation}) => ({
      tabBarIcon: ({}) => {
        const {routeName} = navigation.state;
        if (routeName === 'Search') {
          return <Icon name="search" size={23} />;
        } else if (routeName === 'Message') {
          return <Icon name="comment-alt" size={23} />;
        } else if (routeName === 'Notification') {
          return <Icon name="bell" size={23} />;
        } else {
          return <Icon name="bars" size={23} />;
        }
      },
    }),
    tabBarOptions: {
      activeTintColor: 'red',
    },
  },
);

const Navigator = createStackNavigator(
  {
    Login: {
      screen: Login,
      navigationOptions: {
        header: null,
      },
    },
    SignUpOne: {
      screen: SignUpOne,
      navigationOptions: {
        header: null,
      },
    },
    ZipCodeLogin: {
      screen: ZipCodeLogin,
      navigationOptions: {
        header: null,
      },
    },
    Home: {
      screen: homeBottomTab,
      navigationOptions: {
        header: null,
      },
    },
    Menu: {
      screen: Menu,
      navigationOptions: {
        header: null,
      },
    },
    ViewProfile: {
      screen: ViewProfile,
      navigationOptions: {
        header: null,
      },
    },
    Network: {
      screen: Network,
      navigationOptions: {
        header: null,
      },
    },
    Favourites: {
      screen: Favourites,
      navigationOptions: {
        header: null,
      },
    },
    JobPost: {
      screen: JobPost,
      navigationOptions: {
        header: null,
      },
    },
    Language: {
      screen: Language,
      navigationOptions: {
        header: null,
      },
    },
    Reviews: {
      screen: Reviews,
      navigationOptions: {
        header: null,
      },
    },
    Services: {
      screen: Services,
      navigationOptions: {
        header: null,
      },
    },
    AddPost: {
      screen: AddPost,
      navigationOptions: {
        header: null
      }
    },
    CardProfile: {
      screen: CardProfile,
      navigationOptions: {
        header:null
      }
    },
    VerificationCodeLogin: {
      screen: VerificationCodeLogin,
      navigationOptions: {
        header: null
      }
    },
    ZipCodeSignUp: {
      screen: ZipCodeSignUp,
      navigationOptions: {
        header: null
      }
    },
    VerificationCodeSignUp: {
      screen: VerificationCodeSignUp,
      navigationOptions: {
        header: null
      }
    },
    BasicInfo: {
      screen: BasicInfo,
      navigationOptions: {
        header: null
      }
    },
    BuildTrust: {
      screen: BuildTrust,
      navigationOptions: {
        header: null
      }
    },
    PostService: {
      screen: PostService,
      navigationOptions: {
        header: null
      }
    },
    PostServiceCategory: {
      screen: PostServiceCategory,
      navigationOptions: {
        header: null
      }
    },
    PostServiceDisplay: {
      screen: PostServiceDisplay,
      navigationOptions: {
        header: null
      }
    },
    PostServiceSubDisplay: {
      screen: PostServiceSubDisplay,
      navigationOptions: {
        header: null
      }
    },
    PostServiceLocation: {
      screen: PostServiceLocation,
      navigationOptions: {
        header:null
      }
    },
    PostServicePricing: {
      screen:PostServicePricing,
      navigationOptions: {
        header: null
      }
    },
    PostServiceMinMax: {
      screen: PostServiceMinMax,
      navigationOptions: {
        header: null
      }
    },
    PostServiceReview: {
      screen: PostServiceReview,
      navigationOptions: {
        header: null
      }
    },
    ServicePostDescription: {
      screen: ServicePostDescription,
      navigationOptions: {
        header: null
      }
    }
   
  },
  {
    initialRouteName: 'Home',
  },
);

export default createAppContainer(Navigator);
