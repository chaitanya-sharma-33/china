import React, {Component} from 'react';
import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  Dimensions,
  ScrollView
} from 'react-native';
import Header from '../Header';
import {TabView, SceneMap, TabBar} from 'react-native-tab-view';
import OverviewTab from '../ViewProfile/OverviewTab';
import ServiceTab from '../ViewProfile/ServiceTab';
import JobsTab from '../ViewProfile/JobsTab';
import ReviewTab from '../ViewProfile/ReviewTab';
import NetworkView from '../MenuBody/NetworkView';


import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';

const FirstRoute = () => (
  <View style={{flex: 1}}>
    <ReviewTab />
  </View>
);

const SecondRoute = () => (
  <View style={{flex: 1}}>
     <ReviewTab />

  </View>
);

const ThirdRoute = () => (
  <View style={{flex: 1,marginTop:20}}>
    <ScrollView style={{flex:1}}>
    <NetworkView />
    </ScrollView>
  </View>
);


class Reviews extends Component {
  constructor(props) {
    super(props);
    this.state = {
      index: 0,
      routes: [
        {key: 'first', title: 'ABOUT ME'},
        {key: 'second', title: 'BY ME'},
        {key: 'third', title: 'TO-REVIEW'},
      ],
    };
  }

  render() {
    return (
      <View style={styles.container}>
        <Header
          text="My Reviews"
          name="angle-left"
          onPress={() => this.props.navigation.navigate('Menu')}
          leftIconStyle={{marginLeft: 18}}
          nameRight="sliders-h"
          onPressRight={() => alert('clicked')}
          leftIconStyle={{marginLeft: 18}}
          rightIconStyle={{marginLeft: 170}}
        />

        <View style={styles.bodyContainer}>
        <TabView
            navigationState={this.state}
            renderScene={SceneMap({
              first: FirstRoute,
              second: SecondRoute,
              third: ThirdRoute,
            })}
            onIndexChange={index => this.setState({index})}
            initialLayout={{width: Dimensions.get('window').width}}
            renderTabBar={props => (
              <TabBar
                {...props}
                indicatorStyle={{backgroundColor: '#00b38f'}}
                style={{backgroundColor: 'white'}}
                renderLabel={({route}) => (
                  <Text style={{fontWeight: '500',fontSize:hp("2%")}}>{route.title}</Text>
                )}
              />
            )}
            // swipeEnabled={false}
          />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  bodyContainer: {
    flex: 9,
  },
  scene: {
    flex: 1,
  },
});

export default Reviews;
