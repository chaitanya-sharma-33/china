import React, {Component} from 'react';
import {
  View,
  Text,
  ScrollView,
  TouchableOpacity,
  StyleSheet,
} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome5';
import Card from '../Card';
import NetworkView from '../MenuBody/NetworkView';
import JobPostView from '../MenuBody/JobPostView';
import ReviewBody from '../MenuBody/ReviewBody';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import PostServicePricing from './PostServicePricing';
import PostServiceHeading from './PostServiceHeading';
import PostServiceDetails from './PostServiceDetails';
import PostServiceReviewTab from './PostServiceReviewTab';

class PostServiceOverview extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <View style={{flex: 1}}>
        <ScrollView style={{flex: 1}}>
          <PostServiceHeading headingName="Description" />
          <View>
            <Card>
              <View style={styles.introContainer}>
                <Text>
                  The place is exactly like the pictures. The location is
                  absolutely fabulous and I advise you to try restaurants on
                  their recommended list. We had such an amazing vacation with a
                  great Airbnb, great food and great company.
                </Text>
              </View>
            </Card>
          </View>

          <PostServiceHeading headingName="Pricing" />

          <PostServicePricing />

          <PostServiceHeading headingName="Details" />

          <PostServiceDetails />

          <PostServiceHeading headingName="Reviews" />

          <View style={{flex: 1, marginBottom: 15}}>
            <PostServiceReviewTab />
          </View>
        </ScrollView>
        <View style={styles.footerContainer}>
          <View style={{flex: 4, flexWrap: 'wrap', marginTop: hp('2%')}}>
            <View style={{flexDirection: 'row', alignItems: 'center'}}>
              <View style={{flex: 1, marginLeft: wp('4%')}}>
                <Icon
                  name="dollar-sign"
                  size={15}
                  style={{
                    backgroundColor: '#F5F5F5',
                    borderRadius: 200,
                    padding: 5,
                    alignSelf: 'center',
                  }}
                  color="#4BA6AA"
                />
              </View>
              <View style={{flex: 7}}>
                <Text style={{fontSize: 15}}>
                  110 <Text>/</Text> hour
                </Text>
              </View>
            </View>
            <View
              style={{
                flexDirection: 'row',
                alignItems: 'center',
                marginTop: hp('2%'),
              }}>
              <View style={{flex: 1, marginLeft: wp('4%')}}>
                <Icon
                  name="calendar-alt"
                  size={15}
                  color="#4BA6AA"
                  style={{
                    backgroundColor: '#F5F5F5',
                    borderRadius: 200,
                    padding: 5,
                    alignSelf: 'center',
                  }}
                />
              </View>
              <View style={{flex: 7}}>
                <Text style={{fontSize: 15}}>4.2(20)</Text>
              </View>
            </View>
          </View>
          <View
            style={{
              flex: 3,
              flexWrap: 'wrap',
              justifyContent: 'center',
              marginLeft: wp('1%'),
            }}>
            <TouchableOpacity>
              <View
                style={{
                  flexDirection: 'row',
                  alignItems: 'center',
                  marginLeft: wp('3%'),
                  borderRadius: 400,
                  paddingLeft: 15,
                  backgroundColor: '#F8738B',
                  paddingRight: 15,
                  paddingTop: 10,
                  paddingBottom: 10,
                }}>
                <Icon name="pen" style={{color: 'white'}} size={15} />
                <Text
                  style={{
                    color: 'white',
                    fontSize: hp('2.5%'),
                    marginLeft: wp('2%'),
                  }}>
                  {' '}
                  EDIT POST
                </Text>
              </View>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  introContainer: {
    margin: 15,
  },
  footerContainer: {
    backgroundColor: 'white',
    height: 90,
    elevation: 100,
    flexDirection: 'row',
  },
});

export default PostServiceOverview;
