import React, {Component} from 'react';
import {View, Text, StyleSheet,ScrollView} from 'react-native';
import Card from '../Card';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import Icon from 'react-native-vector-icons/FontAwesome5';

class PostServicePricing extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
       <ScrollView> 
      <Card>
        <View style={styles.verificationContainer}>
          <View
            style={{
              flex: 1,
              flexDirection: 'row',
            }}>
            <View style={{flexDirection: 'row'}}>
              <Icon
                name="clock"
                size={24}
                style={{
                  backgroundColor: '#F5F5F5',
                  borderRadius: 200,
                  padding: 5,
                  alignSelf: 'center',
                }}
              />
            </View>
            <View style={{flex: 6}}>
              <Text style={styles.verificationTextStyle}>Hourly</Text>
              <View style={{flexDirection: 'row', alignItems: 'flex-start'}}>
                <Text
                  style={{
                    fontSize: 16,
                    marginLeft: wp('3%'),
                    color: '#52A7AB',
                  }}>
                  $0 <Text>/</Text>
                </Text>
                <Text
                  style={{
                    fontSize: 16,
                    marginLeft: wp('3%'),
                    color: '#52A7AB',
                  }}>
                  $200
                </Text>
              </View>
            </View>
          </View>
        </View>
        <View style={styles.verificationContainer}>
          <View
            style={{
              flex: 1,
              flexDirection: 'row',
            }}>
            <View style={{flexDirection: 'row'}}>
              <Icon
                name="calendar-week"
                size={24}
                style={{
                  backgroundColor: '#F5F5F5',
                  borderRadius: 200,
                  padding: 5,
                  alignSelf: 'center',
                }}
              />
            </View>
            <View style={{flex: 6}}>
              <Text style={styles.verificationTextStyle}>Daily</Text>
              <View style={{flexDirection: 'row', alignItems: 'flex-start'}}>
                <Text
                  style={{
                    fontSize: 16,
                    marginLeft: wp('3%'),
                    color: '#52A7AB',
                  }}>
                  $500 <Text>/</Text>
                </Text>
                <Text
                  style={{
                    fontSize: 16,
                    marginLeft: wp('3%'),
                    color: '#52A7AB',
                  }}>
                  $800
                </Text>
              </View>
            </View>
          </View>
        </View>
        <View style={styles.verificationContainer}>
          <View
            style={{
              flex: 1,
              flexDirection: 'row',
            }}>
            <View style={{flexDirection: 'row'}}>
              <Icon
                name="calendar-alt"
                size={24}
                style={{
                  backgroundColor: '#F5F5F5',
                  borderRadius: 200,
                  padding: 5,
                  alignSelf: 'center',
                }}
              />
            </View>
            <View style={{flex: 6}}>
              <Text style={styles.verificationTextStyle}>Monthly</Text>
              <View style={{flexDirection: 'row', alignItems: 'flex-start'}}>
                <Text
                  style={{
                    fontSize: 16,
                    marginLeft: wp('3%'),
                    color: '#52A7AB',
                  }}>
                  $10,000 <Text>/</Text>
                </Text>
                <Text
                  style={{
                    fontSize: 16,
                    marginLeft: wp('3%'),
                    color: '#52A7AB',
                  }}>
                  $20,000
                </Text>
              </View>
            </View>
          </View>
        </View>
      </Card>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  verificationContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    margin: 15,
  },
  verificationTextStyle: {
    fontSize: 20,
    marginLeft: wp('3%'),
  },
});

export default PostServicePricing;
