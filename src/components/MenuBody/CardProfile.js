import React, {Component} from 'react';
import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  ScrollView,
  Dimensions,
} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome5';
import Header from '../Header';
import Card from '../Card';
import NetworkView from './NetworkView';
import {TabView, SceneMap, TabBar} from 'react-native-tab-view';
import JobPostView from './JobPostView';
import ReviewBody from './ReviewBody';

const FirstRoute = () => (
  <View style={styles.scene}>
    <ScrollView style={{flex: 1}}>
      <View
        style={{
          flex: 1,
          // backgroundColor: 'orange',
          alignItems: 'center',
          padding: 20,
          flexDirection: 'row',
          justifyContent: 'space-between',
        }}>
        <Text style={{fontSize: 25, marginLeft: 80, marginBottom: 20}}>
          {' '}
          Self Introduction
        </Text>
      </View>
      <View style={{flex: 4, marginTop: -20}}>
        <Card>
          <View>
            <Text style={{margin: 15}}>
              Hello, Here Angie & Chris, we are Parsian and have 2 kids Victor &
              Edouard :)
            </Text>

            <Text style={{margin: 15}}>
              WE design Art and Chic Mini-lofts at cool prices in paris & host
              guestes all over the world.
            </Text>

            <View style={{marginLeft: 10}}>
              <View style={{flexDirection: 'row'}}>
                <Icon
                  name="star"
                  size={15}
                  style={{
                    marginRight: 15,
                    marginBottom: 15,
                    borderRadius: 200,
                    backgroundColor: '#e7e9e9',
                    padding: 5,
                  }}
                />
                <Text style={{fontSize: 15}}>Reviews. 4.2(132)</Text>
              </View>
              <View style={{flexDirection: 'row'}}>
                <Icon
                  name="phone"
                  size={15}
                  style={{
                    marginRight: 15,
                    marginBottom: 15,
                    borderRadius: 200,
                    backgroundColor: '#e7e9e9',
                    padding: 5,
                  }}
                />
                <Text style={{fontSize: 15}}>No. of Connections: 154</Text>
              </View>
              <View style={{flexDirection: 'row'}}>
                <Icon
                  name="question-circle"
                  size={15}
                  style={{
                    marginRight: 15,
                    marginBottom: 15,
                    borderRadius: 200,
                    backgroundColor: '#e7e9e9',
                    padding: 5,
                  }}
                />
                <Text style={{fontSize: 15}}>No. of Enquires: 149</Text>
              </View>
              <View style={{flexDirection: 'row'}}>
                <Icon
                  name="phone"
                  size={15}
                  style={{
                    marginRight: 15,
                    marginBottom: 15,
                    borderRadius: 200,
                    backgroundColor: '#e7e9e9',
                    padding: 5,
                  }}
                />
                <Text style={{fontSize: 15}}>Phone No.: +9595647448</Text>
              </View>
            </View>
          </View>
        </Card>
      </View>

      <View
        style={{
          alignItems: 'center',
          padding: 20,
          flexDirection: 'row',
          justifyContent: 'space-between',
        }}>
        <Text style={{fontSize: 25, marginLeft: 80}}>Verification Info</Text>
      </View>
      <View style={{flex: 1}}>
        <Card>
          <View
            style={{
              flexDirection: 'row',
              margin: 15,
              justifyContent: 'space-between',
            }}>
            <Icon name="globe-asia" size={24} style={{}} />
            <Text style={{fontSize: 20, marginTop: -3, marginLeft: -100}}>
              {' '}
              Facebook
            </Text>
            <TouchableOpacity>
              <Icon name="check" size={22} style={{color:"#00b38f"}}/>
            </TouchableOpacity>
          </View>
          <View
            style={{
              flexDirection: 'row',
              margin: 15,
              justifyContent: 'space-between',
            }}>
            <Icon name="globe" size={20} style={{}} />
            <Text style={{fontSize: 20, marginTop: -3, marginLeft: -120}}>
              {' '}
              Google
            </Text>
            <TouchableOpacity>
              <Icon name="check" size={22} style={{color:"#00b38f"}}/>
            </TouchableOpacity>
          </View>
          <View
            style={{
              flexDirection: 'row',
              margin: 15,
              justifyContent: 'space-between',
            }}>
            <Icon name="id-card" size={20} style={{}} />
            <Text style={{fontSize: 20, marginTop: -3, marginLeft: -120}}>
              {' '}
              ID Card
            </Text>
            <TouchableOpacity>
              <Icon name="check" size={22} style={{color:"#00b38f"}}/>
            </TouchableOpacity>
          </View>
        </Card>
      </View>
      <View
        style={{
          flex: 1,
          alignItems: 'center',
          padding: 20,
          flexDirection: 'row',
          marginTop: -10,
        }}>
        <Text style={{fontSize: 25, marginLeft: 80}}>Services</Text>
        <Icon name="circle" size={4} style={{backgroundColor: "black", borderRadius: 200,marginTop:3,marginLeft: 6}} />
        <Text style={{fontSize:24,marginLeft:6}}>2</Text>
      </View>
      <View style={{flex: 1, marginBottom: 15}}>
        <Card>
          <NetworkView 
          personName="Steve King"
          textUnderIcon="N/A"
          textUnderButtonColor="#717070"
          textLeft={28}
          />
        </Card>
        <Card>
          <NetworkView 
          personName="Steve King"
          textUnderIcon="Available"
          textUnderButtonColor="#009973"
          textLeft={17}
          />
        </Card>
      </View>
      <View
        style={{
          flex: 1,
          alignItems: 'center',
          padding: 10,
          flexDirection: 'row',
          marginTop: -20,
          marginBottom: 10,
          // backgroundColor: "red"
        }}>
        <Text style={{fontSize: 25, marginLeft: 80}}>Job Posts</Text>
        <Icon name="circle" size={4} style={{backgroundColor: "black", borderRadius: 200,marginTop:3,marginLeft: 6}} />
        <Text style={{fontSize:24,marginLeft:6}}>2</Text>
      </View>
      <View style={{flex: 1, marginBottom: 15}}>
        <JobPostView 
         availableText="Urgent"
         firstText="Children in HK"
         buttonColor="#ff6699"
        />

        <JobPostView 
         availableText="Normal"
         firstText="Tutor in Wanchai"
         buttonColor="#1a8cff"
        />
      </View>
      <View
        style={{
          flex: 1,
          alignItems: 'center',
          padding: 10,
          flexDirection: 'row',
          marginTop: -20,
          marginBottom: 10,
          // backgroundColor: "red"
        }}>
        <Text style={{fontSize: 25, marginLeft: 80}}>Reviews</Text>
        <Icon name="circle" size={4} style={{backgroundColor: "black", borderRadius: 200,marginTop:3,marginLeft: 6}} />
        <Text style={{fontSize:23,marginLeft:6}}>4.2 (132)</Text>
      </View>
      <View style={{flex: 1, marginBottom: 15}}>
        <ReviewBody />

        <ReviewBody />
      </View>
    </ScrollView>
    <View
      style={{
        padding: 20,
        elevation: 5,
        backgroundColor: 'white',
        width: '100%',
      }}>
      <View style={{flexDirection: 'row'}}>
        <Icon name="clipboard" size={16} style={{marginRight: 12}} />
        <Text>Services(2)</Text>
        <Icon name="circle" size={3} style={{backgroundColor: "black", borderRadius: 200,marginTop:8,marginLeft: 6,height: 4}} />
        <Text style={{marginLeft:6}}>Jobs(13)</Text>

        <TouchableOpacity
          style={{
            marginLeft: 80,
            backgroundColor: '#ff6699',
            borderRadius: 30,
            paddingLeft: 20,
            paddingRight: 20,
            paddingTop: 15,
            paddingBottom: 15,
            flexDirection: 'row',
          }}>
          <Icon
            name="comment-dots"
            size={20}
            style={{color: '#fff', marginRight: 7}}
          />
          <Text style={{fontSize: 15, color: 'white'}}>MESSAGE</Text>
        </TouchableOpacity>
      </View>
      <View style={{flexDirection: 'row', marginTop: 5}}>
        <Icon
          name="star"
          size={16}
          style={{
            marginRight: 8,
            marginLeft: -3,
            position: 'absolute',
            bottom: 5,
            color: '#009973',
          }}
        />
        <Text style={{position: 'absolute', bottom: 5, marginLeft: 25}}>
          4.2 (20)
        </Text>
      </View>
    </View>
  </View>
);

const SecondRoute = () => (
  <View style={styles.scene}>
    <ScrollView>
      <View
        style={{
          flex: 1,
          alignItems: 'center',
          padding: 20,
          flexDirection: 'row',
        }}>
        <Text style={{fontSize: 25, marginLeft: 80}}>Services</Text>
        <Icon name="circle" size={4} style={{backgroundColor: "black", borderRadius: 200,marginTop:3,marginLeft: 6}} />
        <Text style={{fontSize:24,marginLeft:6}}>2</Text>
      </View>
      <View style={{flex: 1, marginBottom: 15}}>
        <Card>
          <NetworkView 
          personName="Steve King"
          />
        </Card>
        <Card>
          <NetworkView 
          personName="Steve King"
          
          />
        </Card>
      </View>
    </ScrollView>
    <View
      style={{
        padding: 20,
        elevation: 5,
        backgroundColor: 'white',
        width: '100%',
      }}>
      <View style={{flexDirection: 'row'}}>
        <Icon name="clipboard" size={16} style={{marginRight: 12}} />
        <Text>Services(2)</Text>
        <Icon name="circle" size={3} style={{backgroundColor: "black", borderRadius: 200,marginTop:8,marginLeft: 6,height: 4}} />
        <Text style={{marginLeft:6}}>Jobs(13)</Text>
        <TouchableOpacity
          style={{
            marginLeft: 80,
            backgroundColor: '#ff6699',
            borderRadius: 30,
            paddingLeft: 20,
            paddingRight: 20,
            paddingTop: 15,
            paddingBottom: 15,
            flexDirection: 'row',
          }}>
          <Icon
            name="comment-dots"
            size={20}
            style={{color: '#fff', marginRight: 7}}
          />
          <Text style={{fontSize: 15, color: 'white'}}>MESSAGE</Text>
        </TouchableOpacity>
      </View>
      <View style={{flexDirection: 'row', marginTop: 5}}>
        <Icon
          name="star"
          size={16}
          style={{
            marginRight: 8,
            marginLeft: -3,
            position: 'absolute',
            bottom: 5,
            color: '#009973',
          }}
        />
        <Text style={{position: 'absolute', bottom: 5, marginLeft: 25}}>
          4.2 (20)
        </Text>
      </View>
    </View>
  </View>
);

const ThirdRoute = () => (
  <View style={styles.scene}>
    <ScrollView>
      <View
        style={{
          flex: 1,
          alignItems: 'center',
          padding: 20,
          flexDirection: 'row',
        }}>
        <Text style={{fontSize: 25, marginLeft: 80}}>Job Posts</Text>
        <Icon name="circle" size={4} style={{backgroundColor: "black", borderRadius: 200,marginTop:3,marginLeft: 6}} />
        <Text style={{fontSize:24,marginLeft:6}}>2</Text>
      </View>
      <View style={{flex: 1, marginBottom: 15}}>
        <JobPostView 
        availableText="Urgent"
        firstText="Children in HK"
        buttonColor="#ff6699"
        />
        <JobPostView 
         availableText="Normal"
         firstText="Tutor in Wanchai"
         buttonColor= "#1a8cff"
        />
      </View>
    </ScrollView>
    <View
      style={{
        padding: 20,
        elevation: 5,
        backgroundColor: 'white',
        width: '100%',
      }}>
      <View style={{flexDirection: 'row'}}>
        <Icon name="clipboard" size={16} style={{marginRight: 12}} />
        <Text>Services(2)</Text>
        <Icon name="circle" size={3} style={{backgroundColor: "black", borderRadius: 200,marginTop:8,marginLeft: 6,height: 4}} />
        <Text style={{marginLeft:6}}>Jobs(13)</Text>
        <TouchableOpacity
          style={{
            marginLeft: 80,
            backgroundColor: '#ff6699',
            borderRadius: 30,
            paddingLeft: 20,
            paddingRight: 20,
            paddingTop: 15,
            paddingBottom: 15,
            flexDirection: 'row',
          }}>
          <Icon
            name="comment-dots"
            size={20}
            style={{color: '#fff', marginRight: 7}}
          />
          <Text style={{fontSize: 15, color: 'white'}}>MESSAGE</Text>
        </TouchableOpacity>
      </View>
      <View style={{flexDirection: 'row', marginTop: 5}}>
        <Icon
          name="star"
          size={16}
          style={{
            marginRight: 8,
            marginLeft: -3,
            position: 'absolute',
            bottom: 5,
            color: '#009973',
          }}
        />
        <Text style={{position: 'absolute', bottom: 5, marginLeft: 25}}>
          4.2 (20)
        </Text>
      </View>
    </View>
  </View>
);

const FourthRoute = () => (
  <View style={styles.scene}>
    <ScrollView>
      <View
        style={{
          flex: 1,
          alignItems: 'center',
          padding: 10,
          flexDirection: 'row',
          marginBottom: 10,
          marginTop:10
          // backgroundColor: "red"
        }}>
        <Text style={{fontSize: 25, marginLeft: 80}}>Reviews</Text>
        <Icon name="circle" size={4} style={{backgroundColor: "black", borderRadius: 200,marginTop:3,marginLeft: 6}} />
        <Text style={{fontSize:24,marginLeft:6}}>4.2(132)</Text>
      </View>
      <View style={{flex: 1, marginBottom: 15}}>
        <ReviewBody />

        <ReviewBody />
      </View>
    </ScrollView>
    <View
      style={{
        padding: 20,
        elevation: 5,
        backgroundColor: 'white',
        width: '100%',
      }}>
      <View style={{flexDirection: 'row'}}>
        <Icon name="clipboard" size={16} style={{marginRight: 12}} />
        <Text>Services(2)</Text>
        <Icon name="circle" size={3} style={{backgroundColor: "black", borderRadius: 200,marginTop:8,marginLeft: 6,height: 4}} />
        <Text style={{marginLeft:6}}>Jobs(13)</Text>

        <TouchableOpacity
          style={{
            marginLeft: 80,
            backgroundColor: '#ff6699',
            borderRadius: 30,
            paddingLeft: 20,
            paddingRight: 20,
            paddingTop: 15,
            paddingBottom: 15,
            flexDirection: 'row',
          }}>
          <Icon
            name="comment-dots"
            size={20}
            style={{color: '#fff', marginRight: 7}}
          />
          <Text style={{fontSize: 15, color: 'white'}}>MESSAGE</Text>
        </TouchableOpacity>
      </View>
      <View style={{flexDirection: 'row', marginTop: 5}}>
        <Icon
          name="star"
          size={16}
          style={{
            marginRight: 8,
            marginLeft: -3,
            position: 'absolute',
            bottom: 5,
            color: '#009973',
          }}
        />
        <Text style={{position: 'absolute', bottom: 5, marginLeft: 25}}>
          4.2 (20)
        </Text>
      </View>
    </View>
  </View>
);

class CardProfile extends Component {
  constructor(props) {
    super(props);
    this.state = {
      index: 0,
      routes: [
        {key: 'first', title: 'OVERVIEW'},
        {key: 'second', title: 'SERVICES'},
        {key: 'third', title: 'JOBS'},
        {key: 'fourth', title: 'REVIEW'},
      ],
    };
  }

  render() {
    return (
      <View style={styles.container}>
        <Header
          name="angle-left"
          leftIconStyle={{marginLeft: 20}}
          onPress={() => this.props.navigation.navigate('Menu')}
          middleFirstIcon="share-alt"
          middleIconStyle={{marginLeft: 190}}
          middleSecondIcon="bookmark"
          middleSecondIconStyle={{marginLeft: 30}}
          nameRight="ellipsis-v"
          rightIconStyle={{marginLeft: 30}}
        />
        <View style={styles.cardContainer}>
          <Card>
            <NetworkView
              personName="Steve King"
              onPress={() => alert('Clicked')}
            />
          </Card>
        </View>
        <View style={styles.bodyContainer}>
          <TabView
            navigationState={this.state}
            renderScene={SceneMap({
              first: FirstRoute,
              second: SecondRoute,
              third: ThirdRoute,
              fourth: FourthRoute,
            })}
            onIndexChange={index => this.setState({index})}
            initialLayout={{width: Dimensions.get('window').width}}
            renderTabBar={props => (
              <TabBar
                {...props}
                indicatorStyle={{backgroundColor: '#00b38f'}}
                style={{backgroundColor: 'white'}}
                renderLabel={({route}) => (
                  <Text style={{fontWeight: '500'}}>{route.title}</Text>
                )}
              />
            )}
            // swipeEnabled={false}
          />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  cardContainer: {
    flex: 2.7,
    marginTop: 20,
    // backgroundColor: 'orange',
  },
  bodyContainer: {
    flex: 8,
    // backgroundColor: 'red',
  },
  scene: {
    flex: 1,
  },
});

export default CardProfile;
