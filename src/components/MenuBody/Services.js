import React, {Component} from 'react';
import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  ScrollView,
} from 'react-native';
import Header from '../Header';
import Card from '../Card';
import NetworkView from '../MenuBody/NetworkView';

class Services extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <View style={styles.container}>
        <Header
          text="My Services"
          name="angle-left"
          onPress={() => this.props.navigation.navigate('Menu')}
          nameRight="sliders-h"
          onPressRight={() => alert('clicked')}
          leftIconStyle={{marginLeft: 18}}
          rightIconStyle={{marginLeft: 170}}
        />
        <View style={styles.bodyContainer}>
          <ScrollView>
            <NetworkView />
          </ScrollView>
        </View>
        <View style={styles.postContainer}>
          <TouchableOpacity
            style={styles.postTouchable}
            onPress={() => this.props.navigation.navigate('AddPost')}>
            <Text style={styles.postText}>+ POST</Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  bodyContainer: {
    flex: 7,
    marginTop: 20
  },
  postContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  postTouchable: {
    backgroundColor: '#ff6699',
    paddingLeft: 15,
    paddingRight: 15,
    borderRadius: 40,
    paddingTop: 10,
    paddingBottom: 10,
  },
  postText: {color: 'white', fontSize: 20},
});

export default Services;
