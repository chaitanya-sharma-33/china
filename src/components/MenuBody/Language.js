import React, { Component } from 'react';
import { View, Text,StyleSheet,TouchableOpacity } from 'react-native';
import Header from '../Header';


class Language extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  render() {
    return (
      <View style={styles.container}>
      <Header text="Language" name="angle-left" onPress={() => this.props.navigation.navigate("Menu")}/>
     <View style={styles.bodyContainer}>

     </View>
   </View>
    );
  }
}


const styles = StyleSheet.create({
  container: {
    flex:1
  },
  bodyContainer: {
    flex: 9,
    backgroundColor: "orange"
  }
})
export default Language;
