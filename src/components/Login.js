import React, {Component} from 'react';
import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  TextInput,
  Image,
} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome5';
import Header from './Header';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {connect} from 'react-redux';

import {getCodeActionLogin} from '../action';

class Login extends Component {
  constructor(props) {
    super(props);

    const {navigation} = this.props;
    const countryCode = navigation.getParam('code', '+91');
    const code = countryCode.split('+')[1];
    this.state = {
      countryCode: code,
      phoneNumber: '',
    };
  }

  getOTPCode = () => {
    this.props.getCodeActionLogin(this.state);
    this.props.navigation.navigate('VerificationCodeLogin', {
      countryCode: this.state.countryCode,
      phoneNumber: this.state.phoneNumber,
    });
  };

  render() {
    const {navigation} = this.props;
    const countryName = navigation.getParam('countryName', '');
    const code = navigation.getParam('code', 0);

    return (
      <View style={styles.container}>
        <Header
          text="Log In- Verify Phone"
          name="times"
          leftIconStyle={{marginLeft: 18}}
        />
        <View style={styles.bodyContainer}>
          <View style={styles.bodyTextContainer}>
            <Text style={styles.bodyFirstText}>Verify Phone Number</Text>
          </View>
          <View style={styles.bodyZipContainer}>
            <View
              style={{flex: 1, justifyContent: 'center', flexDirection: 'row'}}>
              <Icon
                name="globe"
                size={20}
                style={{
                  backgroundColor: '#F5F5F5',
                  borderRadius: 200,
                  padding: 10,
                }}
              />
            </View>
            <View style={{flex: 5}}>
              <Text style={styles.bodyZipText}> Choose your zip code</Text>

              <View style={{flexDirection: 'row', alignItems: 'center'}}>
                <Text style={styles.bodyZipTextSecond}>
                  {countryName === '' ? 'HONG KONG' : countryName}
                </Text>
                <Text style={styles.bodyZipTextSecond}>
                  {code === 0 ? '+852' : code}
                </Text>
              </View>
            </View>

            <View style={{flex: 2}}>
              <TouchableOpacity
                style={styles.bodyZipIcon}
                onPress={() => this.props.navigation.navigate('ZipCodeLogin')}>
                <Icon name="angle-right" size={20} />
              </TouchableOpacity>
            </View>
          </View>

          <View style={styles.bodyInputContainer}>
            {/* <Text style={{marginLeft:5}}>Phone number</Text> */}
            <TextInput
              style={styles.bodyTextInput}
              placeholder="Phone Number"
              onChangeText={phoneNumber => this.setState({phoneNumber})}
              placeholderTextColor="#188a6e"
            />
          {this.state.phoneNumber === "" ?   <Text style={{margin: 10, color: '#188a6e'}}>
              Enter your phone number
            </Text> : null}
          </View>
          <View
            style={{
              margin: 25,
              flexDirection: 'row',
              justifyContent: 'space-between',
              alignItems: 'center',
            }}>
            <TouchableOpacity
              onPress={() => this.props.navigation.navigate('SignUpOne')}>
              <Text
                style={{
                  fontSize: hp('2.5%'),
                  color: '#298E93',
                  fontWeight: 'bold',
                }}>
                SIGN UP
              </Text>
            </TouchableOpacity>
            {this.state.phoneNumber !== '' ? (
              <TouchableOpacity
                onPress={this.getOTPCode}
                style={{
                  backgroundColor: '#188a6e',
                  borderRadius: 200,
                  padding: 15,
                  alignItems: 'center',
                  flexDirection: 'row',
                }}>
                <Icon name="check" color="white" size={hp('2%')} />
                <Text
                  style={{
                    fontSize: hp('2%'),
                    color: 'white',
                    marginLeft: wp('2%'),
                  }}>
                  GET CODE
                </Text>
              </TouchableOpacity>
            ) : null}
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  bodyContainer: {
    flex: 8,
  },
  bodyTextContainer: {
    alignItems: 'center',
    flex: 2,
    justifyContent: 'center',
    alignItems: 'center',
  },
  bodyFirstText: {
    fontSize: 23,
    fontWeight: '800',
  },
  bodyZipContainer: {
    flex: 2,
    flexDirection: 'row',
    alignItems: 'center',
  },

  bodyZipText: {
    fontSize: hp('3%'),
  },
  bodyZipTextSecond: {
    marginLeft: wp('1.5%'),
    color: '#298E93',
    fontWeight: '600',
  },

  bodyZipIcon: {
    marginLeft: wp('14%'),
  },
  bodyInputContainer: {
    flex: 6,
    marginTop: hp('2%'),
    marginRight: wp('3%'),
    marginLeft: wp('3%'),
  },
  bodyTextInput: {
    borderBottomWidth: 2,
    backgroundColor: '#D0D0D0',
    borderTopLeftRadius: 3,
    borderTopRightRadius: 3,
    borderBottomColor: '#188a6e',
  },
});

export default connect(
  null,
  {getCodeActionLogin},
)(Login);
