import React, {Component} from 'react';
import {View, Text, StyleSheet, TouchableOpacity,ScrollView} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome5';
import Header from './Header';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';

class PostServiceReview extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <View style={styles.container}>
        <Header
          text="Post Service"
          name="times"
          onPress={() => this.props.navigation.navigate('Menu')}
          leftIconStyle={{marginLeft: 18}}
        />

        <View style={styles.textContainer}>
          <Text style={styles.textContainerText}>Childcare</Text>
        </View>
        <View style={styles.textContainer}>
          <Text style={styles.textContainerTextSecond}>Required</Text>
        </View>
        <View style={styles.bodyContainer}>
           <ScrollView> 
          <View style={styles.bodyStyle}>
            <View style={{flex: 1, alignItems: 'center'}}>
              <Icon
                name="location-arrow"
                size={22}
                style={{
                  backgroundColor: '#F5F5F5',
                  borderRadius: 300,
                  padding: 10,
                }}
              />
            </View>
            <View style={{flex: 3}}>
              <Text style={{fontSize: 20, fontWeight: '800'}}>
                Location Type
              </Text>
              <Text style={{fontSize: 15,color: "#2D8F93"}}>Hepler Home</Text>
              <Text style={{fontSize: 15,color: "#2D8F93"}}>New Delhi</Text>

            </View>
            <TouchableOpacity style={{flex: 1}} onPress={() => alert("clicked")}>
              <View style={{alignItems: 'flex-end', marginRight: wp('3%')}}>
                <Icon name="check" size={20} color="#2D8F93"/>
              </View>
            </TouchableOpacity>
          </View>
          <View style={styles.bodyStyle}>
            <View style={{flex: 1, alignItems: 'center'}}>
              <Icon
                name="dollar-sign"
                size={22}
                style={{
                  backgroundColor: '#F5F5F5',
                  borderRadius: 300,
                  padding: 10,
                }}
              />
            </View>
            <View style={{flex: 3}}>
              <Text style={{fontSize: 20, fontWeight: '800'}}>
                Pricing
              </Text>
              <Text style={{fontSize: 15,color: "#2D8F93"}}>Hourly: $0 - $200</Text>
              <Text style={{fontSize: 15,color: "#2D8F93"}}>Daily: $0 - $500</Text>

            </View>
            <TouchableOpacity style={{flex: 1}} onPress={() => alert("clicked")}>
              <View style={{alignItems: 'flex-end', marginRight: wp('3%')}} >
                <Icon name="check" size={20} color= "#2D8F93"/>
              </View>
            </TouchableOpacity>
          </View>
          <View style={{height: 80,justifyContent: "center",alignItems: "center"}}>
          <Text style={{fontSize: 15}}>Optional</Text>
        </View>
        <View style={styles.bodyStyle}>
            <View style={{flex: 1, alignItems: 'center'}}>
              <Icon
                name="table"
                size={22}
                style={{
                  backgroundColor: '#F5F5F5',
                  borderRadius: 300,
                  padding: 10,
                }}
              />
            </View>
            <View style={{flex: 3}}>
              <Text style={{fontSize: 20, fontWeight: '800'}}>
                Scopes
              </Text>

            </View>
            <TouchableOpacity style={{flex: 1}} onPress={() => alert("clicked")}>
              <View style={{alignItems: 'flex-end', marginRight: wp('3%')}} >
                <Icon name="angle-right" size={20}/>
              </View>
            </TouchableOpacity>
          </View>
          <View style={styles.bodyStyle}>
            <View style={{flex: 1, alignItems: 'center'}}>
              <Icon
                name="check-square"
                size={22}
                style={{
                  backgroundColor: '#F5F5F5',
                  borderRadius: 300,
                  padding: 10,
                }}
              />
            </View>
            <View style={{flex: 3}}>
              <Text style={{fontSize: 20, fontWeight: '800'}}>
                Qualifications
              </Text>

            </View>
            <TouchableOpacity style={{flex: 1}} onPress={() => this.props.navigation.navigate("PostService")}>
              <View style={{alignItems: 'flex-end', marginRight: wp('3%')}} >
                <Icon name="angle-right" size={20} />
              </View>
            </TouchableOpacity>
          </View>
          <View style={styles.bodyStyle}>
            <View style={{flex: 1, alignItems: 'center'}}>
              <Icon
                name="pen"
                size={22}
                style={{
                  backgroundColor: '#F5F5F5',
                  borderRadius: 300,
                  padding: 10,
                }}
              />
            </View>
            <View style={{flex: 3}}>
              <Text style={{fontSize: 20, fontWeight: '800'}}>
                Description
              </Text>

            </View>
            <TouchableOpacity style={{flex: 1}} onPress={() => this.props.navigation.navigate("PostService")}>
              <View style={{alignItems: 'flex-end', marginRight: wp('3%')}} >
                <Icon name="angle-right" size={20} />
              </View>
            </TouchableOpacity>
          </View>
          </ScrollView>
          <View
            style={{
              margin: 25,
              flexDirection: 'row',
              justifyContent: 'space-between',
              alignItems: 'center',
            }}>
            <TouchableOpacity>
              <Text
                style={{
                  fontSize: hp('2%'),
                  color: '#188a6e',
                  fontWeight: '800',
                }}>
              ADD MORE SERVICE
              </Text>
            </TouchableOpacity>
            {this.state.location !== '' ? <TouchableOpacity
              style={{
                backgroundColor: '#F8738B',
                borderRadius: 200,
                justifyContent: 'center',
                alignItems: "center",
                paddingTop: 15,
                paddingBottom: 15,
                paddingRight: 20,
                paddingLeft: 20,
                flexDirection: "row"
              }}
              onPress={() => this.props.navigation.navigate("ServicePostDescription")}
              >
            <Icon name="plus" size={18} color="#fff"/>
              <Text
                style={{
                  fontSize: hp('2%'),
                  color: 'white',
                  marginLeft: wp("2%"),
                  fontWeight: "900"
                }}>
                POST
              </Text>
            </TouchableOpacity> : null}
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  textContainer: {
    flex: 1,
    // backgroundColor: "orange",
    justifyContent: 'center',
    alignItems: 'center',
  },
  textContainerTextSecond: {
    fontSize: 17,
    fontWeight: '400',
  },    
  textContainerText: {
    fontSize: 23,
    fontWeight: '400',
  },
  bodyContainer: {
    flex: 7,
    // backgroundColor: "red"
  },
  bodyStyle: {
    flexDirection: 'row',
    // backgroundColor: "orange",
    marginTop: hp('2%'),
    alignItems: 'center',
  },
});

export default PostServiceReview;
