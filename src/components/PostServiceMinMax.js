import React, {Component} from 'react';
import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  TextInput,
  Image,
} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome5';
import Header from './Header';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';

class PostServiceMinMax extends Component {
  constructor(props) {
    super(props);
    this.state = {
      pricemin: '',
      pricemax: '',
      buttonState: true
    };
  }

  render() {
    const {navigation} = this.props;
    const titleLocation = navigation.getParam('titleLocation', '');
    const title = 'Post Service - ' + titleLocation;

    return (
      <View style={styles.container}>
        <Header text={title} name="times" leftIconStyle={{marginLeft: 18}} />
        <View style={styles.bodyContainer}>
          <View style={styles.bodyTextContainer}>
            <Text style={styles.bodyFirstText}>What is hourly pricing ?</Text>
          </View>

          <View style={styles.bodyInputContainer}>
            <TextInput
              style={styles.bodyTextInput}
              placeholder="Minimum"
              placeholderTextColor="#188a6e"
              onChangeText={pricemin => this.setState({pricemin})}
            />
            <Text
              style={{marginLeft: 5, color: '#188a6e', marginBottom: hp('3%')}}>
              Enter your minimum charge in $
            </Text>

            <TextInput
              style={styles.bodyTextInput}
              placeholder="Maximum"
              placeholderTextColor="#188a6e"
              onChangeText={pricemax => this.setState({pricemax})}
            />
            <Text
              style={{marginLeft: 5, color: '#188a6e', marginBottom: hp('3%')}}>
              Enter your maximum charge in $
            </Text>
          </View>
          <View
            style={{
              margin: 25,
              flexDirection: 'row',
              justifyContent: 'space-between',
              alignItems: 'center',
            }}>
            <TouchableOpacity>
              <Text
                style={{
                  fontSize: hp('2%'),
                  color: '#188a6e',
                  fontWeight: '800',
                }}>
                DELETE THIS
              </Text>
            </TouchableOpacity>
            {this.state.pricemin !== '' ? (
              <TouchableOpacity
                style={{
                  backgroundColor: '#237D82',
                  borderRadius: 200,
                  justifyContent: 'center',
                  alignItems: 'center',
                  paddingTop: 15,
                  paddingBottom: 15,
                  paddingRight: 20,
                  paddingLeft: 20,
                  flexDirection: 'row',
                }}
                onPress={() =>
                  this.props.navigation.navigate('PostServicePricing', {
                    pricemin: this.state.pricemin,
                    pricemax: this.state.pricemax,
                    buttonState: this.state.buttonState
                  })
                }>
                <Icon name="check" size={20} color="#fff" />
                <Text
                  style={{
                    fontSize: hp('2%'),
                    color: 'white',
                    marginLeft: wp('2%'),
                  }}>
                  SAVE
                </Text>
              </TouchableOpacity>
            ) : null}
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  bodyContainer: {
    flex: 8,
  },
  bodyTextContainer: {
    alignItems: 'center',
    flex: 2,
    justifyContent: 'center',
    alignItems: 'center',
  },
  bodyFirstText: {
    fontSize: 23,
    fontWeight: '400',
  },
  bodyZipContainer: {
    flex: 2,
    flexDirection: 'row',
    alignItems: 'center',
  },

  bodyZipText: {
    fontSize: hp('3%'),
  },
  bodyZipTextSecond: {
    marginLeft: wp('1.3%'),
  },

  bodyZipIcon: {
    marginLeft: wp('14%'),
  },
  bodyInputContainer: {
    flex: 6,
    marginTop: hp('2%'),
    marginRight: wp('3%'),
    marginLeft: wp('3%'),
  },
  bodyTextInput: {
    borderBottomColor: 'black',
    borderBottomWidth: 2,
    backgroundColor: '#e4e4e4',
    borderTopLeftRadius: 4,
    borderTopRightRadius: 4,
    borderBottomColor: '#188a6e',
  },
});

export default PostServiceMinMax;
