const BASE_URL = 'https://d6.iworklab.com:164/api/v2';
const TOKEN =
  'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1bmlxdWVfbmFtZSI6Ijk1NDA5MTAwODMiLCJuYmYiOjE1NjkyNDc3MTYsImV4cCI6MTU3MTgzOTcxNiwiaWF0IjoxNTY5MjQ3NzE2fQ.OVIc_G5LkqEmXAqc2Oe8c2yXijMW2Gjs6F715Jh_nEs';

export const getCountryCodeAction = () => {
  return async function(dispatch) {
    const data = await fetch(BASE_URL + '/Account/Country?LanguageId=1');
    const jsondata = await data.json();

    dispatch({type: 'FETCH_DATA', payload: jsondata.response.details});
  };
};

export const getCodeActionSignUp = verdata => {
  console.log('getCodeActionSignUp verdata -', verdata);

  return async function(dispatch) {
    const data = await fetch(BASE_URL + '/Account/SendVerificationOTPSignUp', {
      method: 'POST',
      headers: {
        accept: 'text/plain',
        'Content-Type': 'application/json-patch+json',
      },

      body: JSON.stringify(verdata),
    });
    const jsondata = await data.json();
    console.log('getCodeActionSignUp jsondata - ', jsondata);
    dispatch({type: 'GET_CODE', payload: jsondata});
  };
};

export const verifyOTPActionSignUp = args => {
  console.log('verifyOTPAction');
  return async function(dispatch) {
    const data = await fetch(BASE_URL + '/Account/VerifyOTPSignUp', {
      method: 'POST',
      headers: {
        accept: 'text/plain',
        'Content-Type': 'application/json-patch+json',
      },

      body: JSON.stringify(args),
    });
    const jsondata = await data.json();
    console.log(jsondata);
    dispatch({type: 'GET_CODE_SIGNUP', payload: jsondata});
  };
};

export const SignUpAction = args => {
  console.log('SignUpAction', args);
  return async function(dispatch) {
    const data = await fetch(BASE_URL + '/Account/Signup', {
      method: 'POST',
      headers: {
        accept: 'text/plain',
        'Content-Type': 'application/json-patch+json',
      },

      body: JSON.stringify(args),
    });
    const jsondata = await data.json();
    console.log(jsondata);
    dispatch({type: 'GET_CODE_LOGIN', payload: jsondata});
  };
};

export const getCodeActionLogin = verdata => {
  console.log('getcodeSctionLogin - ', verdata);
  return async function(dispatch) {
    const data = await fetch(BASE_URL + '/Account/SendVerificationOTPLogin', {
      method: 'POST',
      headers: {
        accept: 'text/plain',
        'Content-Type': 'application/json-patch+json',
      },

      body: JSON.stringify(verdata),
    });
    const jsondata = await data.json();
    console.log('getcodeActionLogin -', jsondata);
    dispatch({type: 'GET_CODE_LOGIN', payload: jsondata});
  };
};

export const verifyCodeActionLogin = verdata => {
  console.log('VerifyCodeActionLogin', verdata);
  return async function(dispatch) {
    const data = await fetch(BASE_URL + '/Account/VerifyOTPLogin', {
      method: 'POST',
      headers: {
        accept: 'text/plain',
        'Content-Type': 'application/json-patch+json',
      },

      body: JSON.stringify(verdata),
    });
    const jsondata = await data.json();
    console.log('VerifyCodeActionLogin', jsondata);
    dispatch({type: 'VERIFY_CODE_LOGIN', payload: jsondata});
  };
};

export const fetchReviewAction = () => {
  console.log('fetchReviewAction');
  return async function(dispatch) {
    const data = await fetch(BASE_URL + '/Review/GetReviewListAboutMe', {
      method: 'POST',
      headers: {
        accept: 'text/plain',
        'Content-Type': 'application/json-patch+json',
        Authorization: TOKEN,
      },

      body: JSON.stringify({
        languageId: 1,
        pagenumber: 0,
        pagesize: 0,
      }),
    });
    const jsondata = await data.json();
    console.log(jsondata);
    dispatch({type: 'GET_REVIEW', payload: jsondata.response.details.reviews});
  };
};

export const fetchNetworkAction = () => {
  return async function(dispatch) {
    const data = await fetch(BAS0E_URL + '/Network/MyNetwork', {
      method: 'POST',
      headers: {
        accept: 'text/plain',
        'Content-Type': 'application/json-patch+json',
        Authorization: TOKEN,
      },

      body: JSON.stringify({
        languageId: 1,
        pagenumber: 0,
        pagesize: 0,
        sortID: 0,
      }),
    });
    const jsondata = await data.json();
    dispatch({type: 'GET_NETWORK', payload: jsondata.response.details});
  };
};
