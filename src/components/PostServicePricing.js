import React, {Component} from 'react';
import {View, Text, StyleSheet, TouchableOpacity} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome5';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import Header from './Header';

class PostServicePricing extends Component {
  constructor(props) {
    super(props);
    this.state = {
      service: 'Childcare',
    };
  }

  render() {
    const {navigation} = this.props;
    const titleName = navigation.getParam('titleName', '');
    const pricemin = navigation.getParam('pricemin', '');
    const pricemax = navigation.getParam('pricemax', '');
    const buttonState = navigation.getParam('buttonState', '');


    const title = 'Post Service - ' + titleName;
    return (
      <View style={styles.container}>
        <Header
          text={title}
          name="times"
          onPress={() => this.props.navigation.navigate('Menu')}
          leftIconStyle={{marginLeft: 18}}
        />

        <View style={styles.textContainer}>
          <Text style={styles.textContainerText}>What is the pricing ?</Text>
        </View>
        <View style={styles.bodyContainer}>
          <View style={styles.bodyStyle}>
            <View style={{flex: 1, alignItems: 'center'}}>
              <Icon
                name="clock"
                size={22}
                style={{
                  backgroundColor: '#F5F5F5',
                  borderRadius: 300,
                  padding: 10,
                }}
              />
            </View>
            <View style={{flex: 3}}>
              <Text style={{fontSize: 20, fontWeight: '800'}}>Hourly</Text>
              <View style={{flexDirection: 'row', alignItems: 'center'}}>
                <Text
                  style={{fontSize: 14, fontWeight: '800', color: '#188a6e'}}>
                  ${pricemin} -{' '}
                </Text>
                <Text
                  style={{fontSize: 14, fontWeight: '800', color: '#188a6e'}}>
                  ${pricemax} {' '}
                </Text>
              </View>
            </View>

            <View style={{alignItems: 'flex-end', flex: 1}}>
              <TouchableOpacity
                style={{marginRight: wp('8%')}}
                onPress={() =>
                  this.props.navigation.navigate('PostServiceMinMax', {
                    titleLocation: titleName,
                  })
                }>
                <Icon name="angle-right" size={20} />
              </TouchableOpacity>
            </View>
          </View>
          <View style={styles.bodyStyle}>
            <View style={{flex: 1, alignItems: 'center'}}>
              <Icon
                name="calendar-week"
                size={22}
                style={{
                  backgroundColor: '#F5F5F5',
                  borderRadius: 300,
                  padding: 10,
                }}
              />
            </View>
            <View style={{flex: 3}}>
              <Text style={{fontSize: 20, fontWeight: '800'}}>Daily</Text>
              {/* <View style={{flexDirection: 'row', alignItems: 'center'}}>
                <Text
                  style={{fontSize: 14, fontWeight: '800', color: '#188a6e'}}>
                  ${pricemin} -{' '}
                </Text>
                <Text
                  style={{fontSize: 14, fontWeight: '800', color: '#188a6e'}}>
                  ${pricemax} {' '}
                </Text>
              </View> */}
            </View>

            <View style={{alignItems: 'flex-end', flex: 1}}>
              <TouchableOpacity
                style={{marginRight: wp('8%')}}
                onPress={() =>
                  this.props.navigation.navigate('PostServiceMinMax')
                }>
                <Icon name="angle-right" size={20} />
              </TouchableOpacity>
            </View>
          </View>
          <View style={styles.bodyStyle}>
            <View style={{flex: 1, alignItems: 'center'}}>
              <Icon
                name="calendar-alt"
                size={22}
                style={{
                  backgroundColor: '#F5F5F5',
                  borderRadius: 300,
                  padding: 10,
                }}
              />
            </View>
            <View style={{flex: 3}}>
              <Text style={{fontSize: 20, fontWeight: '800'}}>Monthly</Text>
              {/* <View style={{flexDirection: 'row', alignItems: 'center'}}>
                <Text
                  style={{fontSize: 14, fontWeight: '800', color: '#188a6e'}}>
                  ${pricemin} -{' '}
                </Text>
                <Text
                  style={{fontSize: 14, fontWeight: '800', color: '#188a6e'}}>
                  ${pricemax} {' '}
                </Text>
              </View> */}
            </View>

            <View style={{alignItems: 'flex-end', flex: 1}}>
              <TouchableOpacity
                style={{marginRight: wp('8%')}}
                onPress={() =>
                  this.props.navigation.navigate('PostServiceMinMax')
                }>
                <Icon name="angle-right" size={20} />
              </TouchableOpacity>
            </View>
          </View>
          <View style={styles.bodyStyle}>
            <View style={{flex: 1, alignItems: 'center'}}>
              <Icon
                name="dollar-sign"
                size={22}
                style={{
                  backgroundColor: '#F5F5F5',
                  borderRadius: 300,
                  padding: 10,
                }}
              />
            </View>
            <View style={{flex: 3}}>
              <Text style={{fontSize: 20, fontWeight: '800'}}>TBD</Text>
            </View>

            <View style={{alignItems: 'flex-end', flex: 1}}>
              <TouchableOpacity
                style={{marginRight: wp('8%')}}
                onPress={() =>
                  this.props.navigation.navigate('PostServiceMinMax')
                }>
                <Icon name="angle-right" size={20} />
              </TouchableOpacity>
            </View>
          </View>
          
        </View>
            <View style={{flex:1,alignItems: "flex-end",justifyContent: "center"}}>
            {buttonState === true ? <TouchableOpacity
              style={{
                backgroundColor: '#237D82',
                borderRadius: 200,
                paddingTop: 15,
                paddingBottom: 15,
                paddingRight: 20,
                paddingLeft: 20,
                marginRight: wp("4%")
              
              }}
              onPress={() => this.props.navigation.navigate("PostServiceReview")}
              >
             
              <Text
                style={{
                  fontSize: hp('2%'),
                  color: 'white',
                  
                }}>
                NEXT
              </Text>
            </TouchableOpacity> : null}

            </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  bodyContainer: {
    flex: 7,
  },
  textContainer: {
    flex: 1,
    // backgroundColor: "orange",
    justifyContent: 'center',
    alignItems: 'center',
  },
  textContainerText: {
    fontSize: 23,
    fontWeight: '900',
  },
  bodyStyle: {
    flexDirection: 'row',
    // backgroundColor: "orange",
    marginTop: hp('2%'),
    alignItems: 'center',
  },
});

export default PostServicePricing;
