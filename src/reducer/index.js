import { combineReducers } from 'redux';
import ZipCodeReducer from './ZipCodeReducer';
import GetCodeReducer from './GetCodeReducer';
import GetReviewReducer from './GetReviewReducer';
import GetNetworkReducer from './GetNetworkReducer';
import LoginReducer from './LoginReducer';

export default combineReducers({
    display: ZipCodeReducer,
    getSignUpCode: GetCodeReducer,
    reviewReducer: GetReviewReducer,
    networkReducer: GetNetworkReducer,
    login: LoginReducer
});