import React, {Component} from 'react';
import {View, Text, StyleSheet, TouchableOpacity} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome5';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';


class Header extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  render() {
    return (
      <View style={styles.container} {...this.props}>
        <TouchableOpacity onPress={this.props.onPress} style={styles.firstIconContainer}>
        <View style={styles.firstIconContainer}>
         
            <Icon name={this.props.name} size={20} />
         
        </View>
        </TouchableOpacity>
        <View style={styles.headerNameContainer}>
          <Text style={styles.headerNameStyle}> {this.props.text}</Text>
          <Icon name={this.props.dotName} size={5} style={styles.headerIconStyle } />
          <Text style={styles.headerNameStyle}>{this.props.number}</Text>
        </View>
        <View style={styles.lastIconContainer}>
          <TouchableOpacity>
            <Icon name={this.props.middleFirstIcon} size={20} />
          </TouchableOpacity>

          <TouchableOpacity>
            <Icon name={this.props.middleSecondIcon} size={22} />
          </TouchableOpacity>

          <TouchableOpacity onPress={this.props.onPressRight}>
            <Icon name={this.props.nameRight} size={22} />
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'row',
    borderBottomWidth: 3,
    borderBottomColor: '#188a6e',
  },
  firstIconContainer: {
    flex: 1,
    // backgroundColor: 'red',
    alignItems: 'center',
    justifyContent: 'center',
  },
  headerNameContainer: {
    flex: 5,
    // backgroundColor: 'orange',
    flexDirection: 'row',
    alignItems: 'center',
  },
  headerNameStyle: {
    fontSize: hp("2.5%"),
    fontWeight: "800"
  },
  headerIconStyle: {
    backgroundColor: 'black',
    borderRadius: 200,
    margin: '5%',

  },
  lastIconContainer: {
    flex: 3,
    // backgroundColor: 'green',
    alignItems: 'center',
    flexDirection: 'row',
    justifyContent: "space-around"
  },
});

export default Header;
