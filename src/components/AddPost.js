import React, {Component} from 'react';
import {View, Text, StyleSheet, TouchableOpacity} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome5';
import Header from './Header';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';

class AddPost extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <View style={styles.container}>
        <Header
          text="Add Post"
          name="times"
          onPress={() => this.props.navigation.navigate('Menu')}
          leftIconStyle={{marginLeft: 18}}
        />

        <View style={styles.textContainer}>
          <Text style={styles.textContainerText}>Let's post something!</Text>
        </View>
        <View style={styles.bodyContainer}>
          <View style={styles.bodyStyle}>
            <View style={{flex: 1, alignItems: 'center'}}>
              <Icon
                name="clipboard-list"
                size={22}
                style={{
                  backgroundColor: '#F8738B',
                  borderRadius: 300,
                  padding: 10,
                }}
                color="#fff"
              />
            </View>
            <View style={{flex: 3}}>
              <Text style={{fontSize: 23, fontWeight: '800'}}>
                Post service
              </Text>
              <Text style={{fontSize: 17}}>It will not expire</Text>
            </View>
            <TouchableOpacity style={{flex: 1}} onPress={() => this.props.navigation.navigate("PostService")}>
              <View style={{alignItems: 'flex-end', marginRight: wp('3%')}}>
                <Icon name="angle-right" size={20} />
              </View>
            </TouchableOpacity>
          </View>
          <View style={styles.bodyStyle}>
            <View style={{flex: 1, alignItems: 'center'}}>
              <Icon
                name="tags"
                size={22}
                style={{
                  backgroundColor: '#F8738B',
                  borderRadius: 300,
                  padding: 10,
                }}
                color="#fff"
              />
            </View>
            <View style={{flex: 3}}>
              <Text style={{fontSize: 23, fontWeight: '800'}}>
                Post job
              </Text>
              <Text style={{fontSize: 17}}>It will expire in 30 days</Text>
            </View>
            <TouchableOpacity style={{flex: 1}} onPress={() => this.props.navigation.navigate("PostService")}>
              <View style={{alignItems: 'flex-end', marginRight: wp('3%')}} >
                <Icon name="angle-right" size={20} />
              </View>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  textContainer: {
    flex: 1,
    // backgroundColor: "orange",
    justifyContent: 'center',
    alignItems: 'center',
  },
  textContainerText: {
    fontSize: 23,
    fontWeight: '400',
  },
  bodyContainer: {
    flex: 7,
    // backgroundColor: "red"
  },
  bodyStyle: {
    flexDirection: 'row',
    // backgroundColor: "orange",
    marginTop: hp('2%'),
    alignItems: 'center',
  },
});

export default AddPost;
