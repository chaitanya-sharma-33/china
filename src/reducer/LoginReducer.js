export default(state=[], action) => {
    switch (action.type) {
        case "GET_CODE_LOGIN":
            return action.payload
        case "VERIFY_CODE_LOGIN":
            return action.payload    
         default: return state;       
    }
}