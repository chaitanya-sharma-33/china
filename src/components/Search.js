import React, {Component} from 'react';
import {
  View,
  Text,
  StyleSheet,
  Dimensions,
  SafeAreaView,
  TouchableHighlight,
  Animated,
  ScrollView,
  TouchableOpacity
} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome5';
import MapView, {PROVIDER_GOOGLE} from 'react-native-maps';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
// import SelectOption from '../components/SelectOption';
// import ServicesCard from '../components/ServicesCard';

const {width, height} = Dimensions.get('window');
const MARGIN_TOP = height - 205;

class Search extends Component {
  state = {show: false, posY: new Animated.Value(MARGIN_TOP)};

  showView = () => {
    Animated.timing(this.state.posY, {
      toValue: 50,
      duration: 300,
    }).start();
  };

  hideView = () => {
    Animated.timing(this.state.posY, {
      toValue: MARGIN_TOP,
      duration: 200,
    }).start();
  };

  handleShowState = () => {
    console.log('prev state', this.state);
    this.setState({
      show: !this.state.show,
    });

    console.log('new state', this.state);

    if (this.state.show === false) {
      this.hideView();
    } else {
      this.showView();
    }
  };

  render() {
    return (
      <View style={styles.container}>
        <View style={[styles.mapContainer, {width}]}>
          <MapView
            provider={PROVIDER_GOOGLE}
            style={styles.map}
            initialRegion={{
              latitude: 37.78825,
              longitude: -122.4324,
              latitudeDelta: 0.0922,
              longitudeDelta: 0.0421,
            }}
          />

          <View >
            <TouchableHighlight
              onPress={() => this.props.navigation.navigate('AddPost')}
              style={styles.postButton}>
              <Text style={{fontSize: 22, color: 'white'}}> + POST</Text>
            </TouchableHighlight>
          </View>
        </View>
        <Animated.View
          style={[
            styles.dataView,
            {width, marginTop: this.state.posY, height},
          ]}>
          <TouchableOpacity onPress={this.handleShowState}>
            <Icon name="minus" size={26} />
          </TouchableOpacity>
          <View style={styles.area}>
            <Icon name="search" size={22} style={{marginLeft: '3%'}} color="#339498" />
            <Text style={{fontSize: 20,color:"#339498"}}>Honk Kong . 126 Services</Text>
            <Icon name="search-location" size={22} style={{marginRight: '3%'}} color= "#339498"/>
          </View>
          <View style={styles.bottomArea}>
            <ScrollView
              style={{flex: 1,marginTop: hp("2%")}}
              horizontal
              showsHorizontalScrollIndicator={false}>
               <View
                style={{
                  borderRadius: 500,
                  borderWidth: 1,
                  width: 150,
                  justifyContent: 'center',
                  marginLeft: wp('2%'),
                  borderColor: 'grey',
                  height: hp("5%")
                }}>
                <TouchableOpacity
                  style={{
                    flexDirection: 'row',
                    alignItems: 'center',
                    justifyContent: 'space-around',
                  }}>
                  <Icon
                    name="search"
                    style={{
                      backgroundColor: '#339498',
                      borderRadius: 300,
                      padding: 5,
                    }}
                    color="white"
                  />
                  <Text style={{color: '#339498', fontSize: 15}}>
                    Find Helpers
                  </Text>
                  <Icon name="angle-down" color="#339498" size={15} />
                </TouchableOpacity>
              </View>
              <View
                style={{
                  borderRadius: 500,
                  borderWidth: 1,
                  width: 150,
                  justifyContent: 'center',
                  marginLeft: wp('2%'),
                  borderColor: 'grey',
                }}>
                <TouchableOpacity
                  style={{
                    flexDirection: 'row',
                    alignItems: 'center',
                    justifyContent: 'space-around',
                  }}>
                  <Icon
                    name="search"
                    style={{
                      backgroundColor: '#339498',
                      borderRadius: 300,
                      padding: 5,
                    }}
                    color="white"
                  />
                  <Text style={{color: '#339498', fontSize: 15}}>
                    Find Helpers
                  </Text>
                  <Icon name="angle-down" color="#339498" size={15} />
                </TouchableOpacity>
              </View>
              <View
                style={{
                  borderRadius: 500,
                  borderWidth: 1,
                  width: 150,
                  justifyContent: 'center',
                  marginLeft: wp('2%'),
                  borderColor: 'grey',
                }}>
                <TouchableOpacity
                  style={{
                    flexDirection: 'row',
                    alignItems: 'center',
                    justifyContent: 'space-around',
                  }}>
                  <Icon
                    name="search"
                    style={{
                      backgroundColor: '#339498',
                      borderRadius: 300,
                      padding: 5,
                    }}
                    color="white"
                  />
                  <Text style={{color: '#339498', fontSize: 15}}>
                    Find Helpers
                  </Text>
                  <Icon name="angle-down" color="#339498" size={15} />
                </TouchableOpacity>
              </View>
            </ScrollView>
          </View>
        </Animated.View>
      </View>
    );
  }
}
export default Search;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'grey',
    flexDirection: 'column',
  },
  mapContainer: {
    ...StyleSheet.absoluteFillObject,
    height: 550,
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  map: {
    ...StyleSheet.absoluteFillObject,
  },
  dataView: {
    borderRadius: 15,
    backgroundColor: 'white',
    alignItems: 'center',
    elevation: 15,
  },
  postButton: {
    alignSelf: 'center',
    backgroundColor: '#ff80aa',
    height: 50,
    width: 100,
    borderRadius: 50,
    alignItems: 'center',
    justifyContent: 'center',
    marginBottom: hp("20%")
  },
  area: {
    borderColor: 'grey',
    borderWidth: 1,
    width: '80%',
    borderRadius: 50,
    height: 45,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  bottomArea: {
    flexDirection: 'row',
    paddingLeft: '3%',
  },
});
