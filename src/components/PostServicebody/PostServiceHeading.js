import React, { Component } from 'react';
import { View, Text,StyleSheet,TouchableOpacity } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome5';


class PostServiceHeading extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  render() {
    return (
        <View style={styles.headingContainer}>
        <View style={styles.headingNameContainer}>
        <Text style={styles.headingSize}>{this.props.headingName}</Text>
      </View>
      <View style={styles.headingIconContainer}>
        <TouchableOpacity>
          <Icon name="pen" size={20} />
        </TouchableOpacity>
      </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
    headingNameContainer: {
        flex: 7,
        alignItems: 'center',
      },
      headingIconContainer: {
        flex: 1,
        alignItems: 'center',
      },
      headingSize: {
        fontSize: 25,
      },
      headingContainer: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        height: 80,
      },

})

export default PostServiceHeading;
