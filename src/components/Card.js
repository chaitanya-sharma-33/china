import React, {Component} from 'react';
import {View, Text, StyleSheet, TouchableOpacity} from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';

class Card extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return <View style={styles.cardContainer}>{this.props.children}</View>;
  }
}
const styles = StyleSheet.create({
  cardContainer: {
    flex: 1,
    marginLeft: wp("8%"),
    borderWidth: 1,
    borderColor: 'grey',
    marginRight: wp("8%"),
    borderRadius: 7,
  },
});

export default Card;
