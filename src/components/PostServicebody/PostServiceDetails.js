import React, { Component } from 'react'
import { Text, View,StyleSheet,TouchableOpacity,ScrollView } from 'react-native';
import Card from '../Card';
import {
    widthPercentageToDP as wp,
    heightPercentageToDP as hp,
  } from 'react-native-responsive-screen';
  import Icon from 'react-native-vector-icons/FontAwesome5';

export class PostServiceDetails extends Component {
    render() {
        return (
            <ScrollView >
            <Card>
              <View style={styles.verificationContainer}>
                <View
                  style={{
                    flex: 1,
                    flexDirection: 'row',
                  }}>
                  <View style={{flexDirection: 'row'}}>
                    <Icon
                      name="female"
                      size={24}
                      style={{
                        backgroundColor: '#F5F5F5',
                        borderRadius: 200,
                        padding: 5,
                        alignSelf: 'center',
                      }}
                    />
                  </View>
                  <View style={{flex: 6}}>
                    <Text style={styles.verificationTextStyle}>
                      Target age group
                    </Text>
                    <View
                      style={{flexDirection: 'row', alignItems: 'flex-start'}}>
                      <Text
                        style={{
                          fontSize: 16,
                          marginLeft: wp('3%'),
                          color: '#52A7AB',
                        }}>
                        0 <Text>-</Text>
                      </Text>
                      <Text
                        style={{
                          fontSize: 16,
                          marginLeft: wp('3%'),
                          color: '#52A7AB',
                        }}>
                        7
                      </Text>
                    </View>
                  </View>
                </View>
              </View>
              <View style={styles.verificationContainer}>
                <View
                  style={{
                    flex: 1,
                    flexDirection: 'row',
                  }}>
                  <View style={{flexDirection: 'row'}}>
                    <Icon
                      name="check-square"
                      size={24}
                      style={{
                        backgroundColor: '#F5F5F5',
                        borderRadius: 200,
                        padding: 5,
                        alignSelf: 'center',
                      }}
                    />
                  </View>
                  <View style={{flex: 6}}>
                    <Text style={styles.verificationTextStyle}>
                      Years of experience
                    </Text>
                    <Text
                      style={{
                        fontSize: 16,
                        marginLeft: wp('3%'),
                        color: '#52A7AB',
                      }}>
                      14
                    </Text>
                  </View>
                </View>
              </View>
              <View style={styles.verificationContainer}>
                <View
                  style={{
                    flex: 1,
                    flexDirection: 'row',
                    alignItems: 'center',
                  }}>
                  <View style={{flexDirection: 'row'}}>
                    <Icon
                      name="certificate"
                      size={24}
                      style={{
                        backgroundColor: '#F5F5F5',
                        borderRadius: 200,
                        padding: 5,
                        alignSelf: 'center',
                      }}
                    />
                  </View>
                  <View style={{flex: 6}}>
                    <Text style={styles.verificationTextStyle}>
                      Qualification
                    </Text>
                    <View
                      style={{flexDirection: 'row', alignItems: 'flex-start'}}>
                      <Text
                        style={{
                          fontSize: 16,
                          marginLeft: wp('3%'),
                          color: '#52A7AB',
                        }}>
                        Cert 1 <Text>,</Text>
                      </Text>
                      <Text
                        style={{
                          fontSize: 16,
                          marginLeft: wp('3%'),
                          color: '#52A7AB',
                        }}>
                        Cert 2
                      </Text>
                    </View>
                  </View>
                  <View>
                    <Icon name="shield-alt" size={20} color="#F8738B" />
                  </View>
                </View>
              </View>
            </Card>
            </ScrollView>
        )
    }
}

const styles = StyleSheet.create({
    verificationContainer: {
      flexDirection: 'row',
      alignItems: 'center',
      margin: 15,
    },
    verificationTextStyle: {
      fontSize: 20,
      marginLeft: wp('3%'),
    },
  });

export default PostServiceDetails
