import React, {Component} from 'react';
import {
  View,
  Text,
  ScrollView,
  TouchableOpacity,
  StyleSheet,
} from 'react-native';
import {TabView, SceneMap, TabBar} from 'react-native-tab-view';
import Icon from 'react-native-vector-icons/FontAwesome5';
import Card from '../Card';
import NetworkView from '../MenuBody/NetworkView';
import JobPostView from '../MenuBody/JobPostView';
import ReviewBody from '../MenuBody/ReviewBody';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';

class OverviewTab extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <View style={{flex: 1}}>
        <ScrollView style={{flex: 1}}>
          <View style={styles.headingContainer}>
            <View style={styles.headingNameContainer}>
              <Text style={styles.headingSize}>Self Introduction</Text>
            </View>
            <View style={styles.headingIconContainer}>
              <TouchableOpacity>
                <Icon name="pen" size={20} />
              </TouchableOpacity>
            </View>
          </View>
          <View>
            <Card>
              <View style={styles.introContainer}>
                <Text>
                  Hello, Here Angie & Chris, we are Parsian and have 2 kids
                  Victor & Edouard :)
                </Text>

                <Text>
                  WE design Art and Chic Mini-lofts at cool prices in paris &
                  host guestes all over the world.
                </Text>

                <View style={{marginTop: hp("2%")}}> 
                  <View style={styles.introInsideContainer}>
                    <Icon
                      name="star"
                      size={15}
                      style={styles.introInsideIconStyle}
                    />
                    <Text style={styles.introInsideTextStyle}>Reviews. 4.2(132)</Text>
                  </View>
                  <View style={styles.introInsideContainer}>
                    <Icon
                      name="phone"
                      size={15}
                      style={styles.introInsideIconStyle}
                    />
                    <Text style={styles.introInsideTextStyle}>No. of Connections: 154</Text>
                  </View>
                  <View style={styles.introInsideContainer}>
                    <Icon
                      name="question-circle"
                      size={15}
                      style={styles.introInsideIconStyle}
                    />
                    <Text style={styles.introInsideTextStyle}>No. of Enquires: 149</Text>
                  </View>
                  <View style={styles.introInsideContainer}>
                    <Icon
                      name="phone"
                      size={15}
                      style={styles.introInsideIconStyle}
                    />
                    <Text style={styles.introInsideTextStyle}>Phone No.: +9595647448</Text>
                  </View>
                </View>
              </View>
            </Card>
          </View>

          <View
            style={styles.headingContainer}>
             <View style={styles.headingNameContainer}>
              <Text style={styles.headingSize}>Verification Info</Text>
            </View>
            <View style={styles.headingIconContainer}>
              <TouchableOpacity>
                <Icon name="pen" size={20} />
              </TouchableOpacity>
            </View>
          </View>
          <View>
            <Card>
              <View style={styles.verificationContainer}>
               
               <View style={{flex:6,flexDirection: "row"}}>
               <Icon name="globe-asia" size={24}  />
                <Text style={styles.verificationTextStyle}>
                  Facebook
                </Text>
               </View>
               <View style={{flex: 1}}>
                <TouchableOpacity>
                  <Icon name="check" size={22} style={{color: '#00b38f'}} />
                </TouchableOpacity>
                </View>
              </View>
              <View style={styles.verificationContainer}>
               
              <View style={{flex:6,flexDirection: "row"}}>
               <Icon name="globe-asia" size={24}  />
                <Text style={styles.verificationTextStyle}>
                  Facebook
                </Text>
               </View>
               <View style={{flex: 1}}>
                <TouchableOpacity>
                  <Icon name="check" size={22} style={{color: '#00b38f'}} />
                </TouchableOpacity>
                </View>
              </View>
              <View style={styles.verificationContainer}>
                 
               <View style={{flex:6,flexDirection: "row"}}>
               <Icon name="globe-asia" size={24}  />
                <Text style={styles.verificationTextStyle}>
                  Facebook
                </Text>
               </View>
               <View style={{flex: 1}}>
                <TouchableOpacity>
                  <Icon name="check" size={22} style={{color: '#00b38f'}} />
                </TouchableOpacity>
                </View>
              </View>
            </Card>
          </View>
          <View
            style={styles.headingContainer}>
          <View style={styles.headingNameContainer}>
              <Text style={styles.headingSize}>Services</Text>
            </View>
            <View style={styles.headingIconContainer}>
              <TouchableOpacity>
                <Icon name="pen" size={20} />
              </TouchableOpacity>
            </View>
          </View>
          <View>
            <NetworkView />
            <NetworkView />
          </View>
          <View style={styles.headingContainerJob}>
          <View style={styles.headingNameContainer}>
              <Text style={styles.headingSize}>Job Post</Text>
            </View>
            <View style={styles.headingIconContainer}>
              <TouchableOpacity>
                <Icon name="pen" size={18} style={{marginTop: 5}}/>
              </TouchableOpacity>
            </View>
          </View>
          <View style={{flex: 1, marginBottom: 15}}>
            <JobPostView
              availableText="Urgent"
              firstText="Children in HK"
              buttonColor="#ff6699"
            />

            <JobPostView
              availableText="Normal"
              firstText="Tutor in Wanchai"
              buttonColor="#1a8cff"
            />
          </View>
          <View style={styles.headingContainerReview}>
          <View style={styles.headingNameContainer}>
              <Text style={styles.headingSize}>Reviews</Text>
            </View>
            <View style={styles.headingIconContainer}>
              <TouchableOpacity>
                <Icon name="pen" size={18} style={{marginTop: 5}} />
              </TouchableOpacity>
            </View>
          </View>
          <View style={{flex: 1, marginBottom: 15}}>
            <ReviewBody />
          </View>
        </ScrollView>
      <View style={styles.footerContainer}>
      <View style={{flex:4,flexWrap: "wrap",marginTop: hp("2%")}}>
         <View style={{flexDirection: "row",alignItems: "center"}}>
         <View style={{flex:1,marginLeft: wp("4%")}}>
            <Icon name="clipboard" size={17} />
          </View>
          <View style={{flex: 7}}>
              <Text style={{fontSize: 15}}>Servies(2) <Text>.</Text> Jobs(13)</Text>
          </View>
         </View>
         <View style={{flexDirection: "row",alignItems: "center",marginTop: hp("2%")}}>
         <View style={{flex:1,marginLeft: wp("4%")}}>
            <Icon name="star" size={15} />
          </View>
          <View style={{flex: 7}}>
              <Text style={{fontSize: 15}}>4.2(20)</Text>
          </View>
         </View>
        </View>
        <View style={{flex:3 ,flexWrap: "wrap",justifyContent: "center",marginLeft: wp("1%")}}>
          <TouchableOpacity>
          <View style={{flexDirection: "row",alignItems: "center",marginLeft: wp("3%"),borderRadius: 300,paddingLeft: 15,backgroundColor:"red",paddingRight: 15,paddingTop:10,paddingBottom:10}}>
          <Icon name="pen" style={{color: "white"}} size={15}/>
            <Text style={{color: "white",fontSize: hp("2.5%")}}> Message</Text>
          </View>
          </TouchableOpacity>
        </View>

      </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  headingContainer: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    height: 80,
  },
  headingContainerJob: {
    flexDirection: 'row',
    height: 55,
  },
  headingContainerReview: {
    flexDirection: 'row',
    height: 50,
  },
  headingNameContainer: {
    flex: 7,
    alignItems: 'center',
  },
  headingIconContainer: {
    flex: 1,
    alignItems: 'center',
  },
  introContainer: {
    margin: 15,
  },
  headingSize: {
    fontSize: 25
  },

  introInsideContainer: {
    flexDirection: "row",
    alignItems: "center",
    margin:5
  },
  introInsideIconStyle: {
    borderRadius: 200,
    backgroundColor: '#e7e9e9',
    padding: 5,
    marginRight: wp("2%")
  },
  introInsideTextStyle:{
    fontSize: 15
  },
  verificationContainer: {
    flexDirection: "row",
    alignItems: "center",
    margin: 15
  },
  verificationTextStyle: {
    fontSize: 20,
    marginLeft: wp("3%")
  },
  footerContainer: {
    backgroundColor: "white",
    height: 80,
    elevation: 100,
    flexDirection: "row",

  }
});

export default OverviewTab;
