import React from 'react';
import {View,StyleSheet} from 'react-native';
import Navigator from './src/navigation';
import {createStore,applyMiddleware} from 'redux';
import { Provider} from 'react-redux';
import ReduxThunk from 'redux-thunk';
import reducers from './src/reducer';

const store = createStore(reducers,applyMiddleware(ReduxThunk))

class App extends React.Component {
  render() {
    return (
      <View style={styles.container}>
        <Provider store={store}>
        <Navigator />
        </Provider>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container:{
    flex:1
  }
})

export default App;
