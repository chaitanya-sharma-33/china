import React, {Component} from 'react';
import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  ScrollView,
} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome5';
import Card from '../Card';
import NetworkView from '../MenuBody/NetworkView';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
class ServiceTab extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }



  render() {
    return (
      <View style={{flex: 1}}>
        <ScrollView >
          <View
            style={{
              flex: 1,
              alignItems: 'center',
              padding: 20,
              flexDirection: 'row',
            }}>
            <Text style={{fontSize: 25, marginLeft: 80}}>Services</Text>

            <Icon
              name="circle"
              size={4}
              style={{
                backgroundColor: 'black',
                borderRadius: 200,
                marginTop: 3,
                marginLeft: 6,
              }}
            />
            <Text style={{fontSize: 24, marginLeft: 6}}>2</Text>
            <TouchableOpacity style={{marginLeft: 145}}>
              <Icon name="pen" size={20} />
            </TouchableOpacity>
          </View>
          <View style={{flex: 1, marginBottom: 15}}>
            <NetworkView  />
          </View>
        </ScrollView>
      <View style={styles.footerContainer}>
      <View style={{flex:4,flexWrap: "wrap",marginTop: hp("2%")}}>
         <View style={{flexDirection: "row",alignItems: "center"}}>
         <View style={{flex:1,marginLeft: wp("4%")}}>
            <Icon name="clipboard" size={17} />
          </View>
          <View style={{flex: 7}}>
              <Text style={{fontSize: 15}}>Servies(2) <Text>.</Text> Jobs(13)</Text>
          </View>
         </View>
         <View style={{flexDirection: "row",alignItems: "center",marginTop: hp("2%")}}>
         <View style={{flex:1,marginLeft: wp("4%")}}>
            <Icon name="star" size={15} />
          </View>
          <View style={{flex: 7}}>
              <Text style={{fontSize: 15}}>4.2(20)</Text>
          </View>
         </View>
        </View>
        <View style={{flex:3 ,flexWrap: "wrap",justifyContent: "center",marginLeft: wp("1%")}}>
          <TouchableOpacity>
          <View style={{flexDirection: "row",alignItems: "center",marginLeft: wp("3%"),borderRadius: 300,paddingLeft: 15,backgroundColor:"red",paddingRight: 15,paddingTop:10,paddingBottom:10}}>
          <Icon name="pen" style={{color: "white"}} size={15}/>
            <Text style={{color: "white",fontSize: hp("2.5%")}}> Message</Text>
          </View>
          </TouchableOpacity>
        </View>

      </View>
      </View>
    );
  }
}


const styles = StyleSheet.create({
  footerContainer: {
    backgroundColor: "white",
    height: 80,
    elevation: 100,
    flexDirection: "row",

  }
})


export default ServiceTab;
