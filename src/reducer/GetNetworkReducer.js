export default (state = [], action) => {
  switch (action.type) {
    case 'GET_NETWORK':
      return action.payload;
    default:
      return state;
  }
};
