import React, { Component } from 'react';
import { View, Text,StyleSheet,TouchableOpacity } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome5';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import Header from './Header';

class PostServiceCategory extends Component {
  constructor(props) {
    super(props);
    this.state = {
        service: "Childcare"
    };
   
  }

  render() {
    const {navigation} = this.props;
    const titleName = navigation.getParam('titleName', '');
      const  title="Post Service - " + titleName
    return (
      <View style={styles.container}>
           <Header
          text= {title}
          name="times"
          onPress={() => this.props.navigation.navigate('Menu')}
          leftIconStyle={{marginLeft: 18}}
        />
        
        <View style={styles.textContainer}>
          <Text style={styles.textContainerText}>
            What is the location type ?
          </Text>
        </View>
        <View style={styles.bodyContainer}>
        <View style={styles.bodyStyle}>
            <View style={{flex: 1, alignItems: 'center'}}>
              <Icon
                name="home"
                size={22}
                style={{
                  backgroundColor: '#F8738B',
                  borderRadius: 300,
                  padding: 10,
                }}
                color="#fff"
              />
            </View>
            <View style={{flex: 3}}>
              <Text style={{fontSize: 20, fontWeight: '800'}}>Helper's Home</Text>
              <Text style={{fontSize: 17, fontWeight: '800'}}>To help at your home place</Text>

            </View>

            <View style={{alignItems: 'flex-end', flex: 1}}>
              <TouchableOpacity style={{marginRight: wp("8%")}} onPress={() => this.props.navigation.navigate("PostServiceLocation", {
                titleLocation: titleName
              })}>
                  <Text>Select</Text>
              </TouchableOpacity>
            </View>
          </View>
          <View style={styles.bodyStyle}>
            <View style={{flex: 1, alignItems: 'center'}}>
              <Icon
                name="running"
                size={22}
                style={{
                  backgroundColor: '#4284F4',
                  borderRadius: 300,
                  padding: 10,
                }}
                color="#fff"
              />
            </View>
            <View style={{flex: 3}}>
              <Text style={{fontSize: 20, fontWeight: '800'}}>Mobile Helper</Text>
              <Text style={{fontSize: 17, fontWeight: '800'}}>Yo go out and help</Text>

            </View>

            <View style={{alignItems: 'flex-end', flex: 1}}>
              <TouchableOpacity style={{marginRight: wp("8%")}}  onPress={() => this.props.navigation.navigate("PostServiceLocation")}>
                  <Text>Select</Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
       
      </View>
    );
  }
}

const styles = StyleSheet.create({
    container: {
            flex:1
    },
    bodyContainer: {
        flex:7,
        
    },
    textContainer: {
        flex: 1,
        // backgroundColor: "orange",
        justifyContent: 'center',
        alignItems: 'center',
      },
      textContainerText: {
        fontSize: 23,
        fontWeight: '900',
      },
      bodyStyle: {
        flexDirection: 'row',
        // backgroundColor: "orange",
        marginTop: hp('2%'),
        alignItems: 'center',
      },
})


export default PostServiceCategory;
