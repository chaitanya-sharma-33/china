import React, {Component} from 'react';
import {
  View,
  StyleSheet,
  ScrollView,
} from 'react-native';
import Header from '../Header';
import NetworkView from '../MenuBody/NetworkView';

class Network extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <View style={styles.container}>
        <Header
          text="My Network"
          name="angle-left"
          onPress={() => this.props.navigation.navigate('Menu')}
          nameRight="sliders-h"
          onPressRight={() => alert('clicked')}
          leftIconStyle={{marginLeft: 18}}
          rightIconStyle={{marginLeft: 150,marginTop:3}}
          iconDot="circle"
          iconDotStyle={{marginLeft:5,marginTop:3,borderRadius: 200,backgroundColor: "black",}}
          numberText="2"
        />
        <View style={styles.bodyContainer}>
          <ScrollView>
            <NetworkView />
          </ScrollView>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  bodyContainer: {
    flex: 9,
    marginTop: 20
    // backgroundColor: 'orange',
  },
});

export default Network;
