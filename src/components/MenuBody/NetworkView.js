import React, {Component} from 'react';
import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  ActivityIndicator,
  Image
} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome5';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {fetchNetworkAction} from '../../action';
import {connect} from 'react-redux';

class NetworkView extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  componentDidMount = () => {
    this.props.fetchNetworkAction();
  };
  renderNetwork = () => {
    if (this.props.networkReducer.length === 0) {
      return <ActivityIndicator size="large" color="blue" />;
    } else {
      return this.props.networkReducer.map((item, i) => {
        const imageUri =
          'https://d6.iworklab.com:162/myimages/' +
          item.userData.thumnailProfileImageURL;
        return (
          <View key={i} style={styles.networkDataContainer}>
            <TouchableOpacity style={{flexDirection: 'row'}}>
              <View style={styles.IconViewContainer}>
                <Icon
                  name="user-check"
                  size={10}
                  style={styles.userCheckStyle}
                />
                {/* <Icon
                  name="user-circle"
                  size={35}
                  style={styles.userCircleStyle}
                /> */}
                <Image source={{uri: imageUri}} style={styles.userCircleStyle} />
                <Text style={styles.userTextStyle}>N/A</Text>
              </View>
              <View style={styles.bodyContainer}>
                <View style={styles.bodyInnerContainer}>
                  <Text style={styles.personNameStyle}>
                    {item.userData.userName}
                  </Text>
                  <Icon
                    name="check-circle"
                    size={13}
                    style={styles.personNameIcon}
                  />
                </View>
                <View style={styles.bodyInnerContainer}>
                  <Icon name="clipboard-list" />
                  <View style={{flexDirection: 'row'}}>
                    <Text style={styles.innerTextStyle}>Services</Text>
                    <Text style={styles.innerTextStyle}>
                      ({item.otherDetail.userprofiletotal.serviceTotal})
                    </Text>
                  </View>
                  <Icon name="circle" size={4} style={styles.circleStyle} />
                  <View style={{flexDirection: 'row'}}>
                    <Text style={styles.innerTextStyle}>Jobs </Text>
                    <Text style={styles.innerTextStyle}>({item.otherDetail.userprofiletotal.jobTotal})</Text>
                  </View>
                </View>
                <View style={styles.bodyInnerContainer}>
                  <Icon name="map-marker-alt" />
                  <Text style={styles.innerTextStyle}>Lippo Center</Text>
                </View>
                <View style={styles.bodyInnerContainer}>
                  <Icon name="star" />
                  <View style={{flexDirection: 'row'}}>
                    <Text style={styles.innerTextStyle}>
                      {item.otherDetail.userProfileInfoSection.rating} .
                    </Text>
                    <Text style={{}}>
                      ({item.otherDetail.userProfileInfoSection.noOfConnections}
                      )
                    </Text>
                  </View>
                </View>
              </View>
              <View style={styles.lastIconContainer}>
                <Icon name="ellipsis-v" size={20} />
              </View>
            </TouchableOpacity>
          </View>
        );
      });
    }
  };

  render() {
    return <View style={styles.container}>{this.renderNetwork()}</View>;
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  networkDataContainer: {
    flex: 1,
    marginLeft: wp('8%'),
    borderWidth: 1,
    borderColor: 'grey',
    marginRight: wp('8%'),
    borderRadius: 7,
    marginBottom: hp('3%'),
    justifyContent: 'center',
  },
  IconViewContainer: {
    flex: 2.7,
    alignItems: 'center',
  },
  userCircleStyle: {
    position: 'absolute',
    borderRadius: 300,
    marginTop: hp('3%'),
    width: 65,
    height: 65,
  },
  userCheckStyle: {
    backgroundColor: '#009973',
    marginTop: hp('1%'),
    marginLeft: wp('12%'),
    borderRadius: 300,
    padding: 10,
    zIndex: 1,
    color: 'white',
  },
  userTextStyle: {
    backgroundColor: 'green',
    marginLeft: wp('1.3%'),
    borderRadius: 200,
    paddingLeft: 15,
    paddingRight: 15,
    color: 'white',
    marginTop: hp('5.7%'),
  },
  bodyContainer: {
    flex: 7,
    marginBottom: hp('1%'),
  },
  personNameStyle: {
    fontSize: 25,
    fontWeight: '700',
  },
  personNameIcon: {
    color: 'red',
    marginLeft: wp('2%'),
  },
  innerTextStyle: {
    marginLeft: wp('2%'),
  },
  circleStyle: {
    backgroundColor: 'black',
    borderRadius: 300,
    height: '18%',
    marginLeft: wp('2%'),
  },
  innerIconStyleStar: {
    marginLeft: -2,
    marginBottom: hp('4%'),
  },
  lastIconContainer: {
    marginTop: hp('2.5%'),
    marginRight: wp('2%'),
  },
  bodyInnerContainer: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    alignItems: 'center',
    marginLeft: wp('4%'),
    marginTop: hp('1.5%'),
  },
});

const mapStateToProps = state => {
  return {
    networkReducer: state.networkReducer,
  };
};

export default connect(
  mapStateToProps,
  {fetchNetworkAction},
)(NetworkView);
