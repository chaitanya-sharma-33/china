import React, {Component} from 'react';
import {View, Text, StyleSheet, Dimensions,ScrollView} from 'react-native';
import Header from './Header';
import NetworkView from './MenuBody/NetworkView';
import {TabView, SceneMap, TabBar} from 'react-native-tab-view';
import OverviewTab from './ViewProfile/OverviewTab';
import ServiceTab from './ViewProfile/ServiceTab';
import JobsTab from './ViewProfile/JobsTab';
import ReviewTab from './ViewProfile/ReviewTab';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';


const FirstRoute = () => (
  <View style={{flex: 1}}>
    <OverviewTab />
  </View>
);

const SecondRoute = () => (
  <View style={{flex: 1}}>
    <ServiceTab />
  </View>
);

const ThirdRoute = () => (
  <View style={{flex: 1}}>
    <JobsTab />
  </View>
);

const FourthRoute = () => (
  <View style={{flex: 1}}>
    <ReviewTab />
  </View>
);

class ViewProfile extends Component {
  constructor(props) {
    super(props);
    this.state = {
      index: 0,
      routes: [
        {key: 'first', title: 'OVERVIEW'},
        {key: 'second', title: 'SERVICES'},
        {key: 'third', title: 'JOBS'},
        {key: 'fourth', title: 'REVIEW'},
      ],
    };
  }

  render() {
    return (
      <View style={styles.container}>
        <Header
          name="angle-left"
          onPress={() => this.props.navigation.navigate('Menu')}
          middleFirstIcon="share-alt"
          middleSecondIcon="bookmark"
          nameRight="ellipsis-v"
        />
        <View style={styles.cardContainer}>
        
        </View>
        <View style={styles.bodyContainer}>
          <TabView
            navigationState={this.state}
            renderScene={SceneMap({
              first: FirstRoute,
              second: SecondRoute,
              third: ThirdRoute,
              fourth: FourthRoute,
            })}
            onIndexChange={index => this.setState({index})}
            initialLayout={{width: Dimensions.get('window').width}}
            renderTabBar={props => (
              <TabBar
                {...props}
                indicatorStyle={{backgroundColor: '#00b38f'}}
                style={{backgroundColor: 'white'}}
                renderLabel={({route}) => (
                  <Text style={{fontWeight: '500',fontSize:hp("2%")}}>{route.title}</Text>
                )}
              />
            )}
            // swipeEnabled={false}
          />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  cardContainer: {
    flex: 3,
    marginTop: 20,
    // backgroundColor: 'orange',
  },
  bodyContainer: {
    flex: 7,
    // backgroundColor: 'red',
  },
  scene: {
    flex: 1,
  },
});

export default ViewProfile;
