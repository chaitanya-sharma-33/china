import React, {Component} from 'react';
import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  Dimensions,
  ScrollView,
} from 'react-native';
import Header from '../Header';
import {TabView, SceneMap, TabBar} from 'react-native-tab-view';
import Card from '../Card';
import NetworkView from '../MenuBody/NetworkView';
import JobPostView from './JobPostView';


const FirstRoute = () => (
  <View style={styles.scene}>
    <ScrollView>
      <NetworkView />

      <NetworkView />

      <NetworkView />
    </ScrollView>
  </View>
);

const SecondRoute = () => (
  <View style={styles.scene}>
   <ScrollView>
    <JobPostView />
    <JobPostView />
    <JobPostView />
    <JobPostView />

    </ScrollView>
  </View>
);

class Favourites extends Component {
  constructor(props) {
    super(props);
    this.state = {
      index: 0,
      routes: [
        {key: 'first', title: 'SERVICES'},
        {key: 'second', title: 'JOBS'},
      ],
    };
  }

  render() {
    return (
      <View style={styles.container}>
        <Header
          text="My Favourites"
          name="angle-left"
          onPress={() => this.props.navigation.navigate('Menu')}
          leftIconStyle={{marginLeft: 18}}
        />
        <View style={styles.bodyContainer}>
          <TabView
            navigationState={this.state}
            renderScene={SceneMap({
              first: FirstRoute,
              second: SecondRoute,
            })}
            onIndexChange={index => this.setState({index})}
            initialLayout={{width: Dimensions.get('window').width}}
            renderTabBar={props => (
              <TabBar
                {...props}
                indicatorStyle={{backgroundColor: '#00b38f'}}
                style={{backgroundColor: 'white'}}
                renderLabel={({route}) => (
                  <Text style={{fontWeight: '500'}}>{route.title}</Text>
                )}
              />
            )}
            // swipeEnabled={false}
          />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  bodyContainer: {
    flex: 9,
    marginTop: 5,
    // backgroundColor: 'orange',
  },
  scene: {
    flex: 1,
    marginTop: 20,
  },
});

export default Favourites;
