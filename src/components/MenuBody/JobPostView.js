import React, {Component} from 'react';
import {View, Text, StyleSheet, TouchableOpacity} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome5';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';

class JobPostView extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      
        <View style={styles.container}>
          <TouchableOpacity style={{flexDirection: 'row',}}>
          <View style={styles.IconViewContainer}>
              <Text style={{backgroundColor:"pink",borderRadius: 200,paddingLeft: wp("2%"),paddingRight: wp("2%"),marginTop: hp("3%")}}>
                Urgent
              </Text>
          </View>
          <View style={styles.bodyContainer}>
            <View style={styles.bodyInnerContainer}>
              <Text style={styles.personNameStyle}>Childcare in HK</Text>
            </View>
            <View style={styles.bodyInnerContainer}>
              <Icon name="clipboard-list"  />
              <Text style={styles.innerTextStyle}>Childcare</Text>
              <Icon name="circle" size={4} style={styles.circleStyle} />
              <Text style={styles.innerTextStyle}>$100/hour</Text>
            </View>
            <View style={styles.bodyInnerContainer}>
              <Icon name="map-marker-alt"  />
              <Text style={styles.innerTextStyle}>Lippo Center</Text>
            </View>
            <View style={styles.bodyInnerContainer}>
              <Icon name="calendar-week" />
              <Text style={styles.innerTextStyle}>Expires in 28 days</Text>
            </View>
            <View style={styles.bodyInnerContainer}>
              <Icon name="calendar-week" />
              <Text style={styles.innerTextStyle}>Posted by <Text style={{color: "green"}}>Marry Ann</Text></Text>
            </View>
          </View>
          <View style={styles.lastIconContainer}>
            <Icon name="ellipsis-v" size={18} />
          </View>
          </TouchableOpacity>
        </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginLeft: wp('8%'),
    borderWidth: 1,
    borderColor: 'grey',
    marginRight: wp('8%'),
    borderRadius: 7,
    marginBottom: hp('2%'),
    justifyContent: "center", 
  },
  IconViewContainer: {
    flex: 2.7,
    alignItems: "center"
  },
  bodyContainer: {
    flex: 7,
    marginBottom: hp("1%")
  },
  personNameStyle: {
    fontSize: hp("3%"),
    fontWeight: '500',
  },
  personNameIcon: {
    color: 'red',
    marginLeft: wp('2%'),
  },
  innerTextStyle: {
    marginLeft: wp("2%"),
  },
  circleStyle: {
    backgroundColor: 'black',
    borderRadius: 300,
    height: '18%',
   marginLeft: wp("2%")
  },
  innerIconStyleStar: {
      marginLeft: -2,
      marginBottom: hp("4%")

  },
  lastIconContainer: {
    marginTop: hp('2.5%'),
    marginRight: wp("2%")
  },
  bodyInnerContainer: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    alignItems: "center",
    marginLeft: wp("4%"),
    marginTop: hp("1.5%"),
  },
});

export default JobPostView;
